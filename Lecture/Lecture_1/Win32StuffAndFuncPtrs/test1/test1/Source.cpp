// Professor Dellafelice
// CP240 Examples of 
//	precompiler #define, and macro, and masking bits (LOWORD)
//	function pointers - similar to Win32 callback functions
//						and C# delegates
//		

#include <iostream>
using namespace std;

#define FALSE 0
#define TRUE  1
typedef int BOOL;

BOOL b = TRUE;

// For Win32 these are #define, and part of Windows.h 
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned int DWORD;

// Example of a Macro
#define AS_BYTE(b) ((int)b & 0xff)

// #define works too
typedef unsigned short PLID;
//#define PLID unsigned short

struct PLAYER
{
	PLID id;
	int score;
};

void getPlayerScores(PLID id, int *scores, int nScores)
{
	cout << "Player " << id << " scores\n";
	for (int i = 0; i < nScores; i++)
	{
		cout << "Score#" << i << '\t' << scores[i] << endl;
	}
}

int sub(int a, int b) { return (a - b); }
int add(int a, int b) { return (a + b); }

void addWithReport(int a, int b, void (*reportBack)(int r))
{
	int sum = a + b;
	reportBack(sum);
}

void done(int result)
{
	cout << "\nThe result is: " << result << endl;
}

// ** Pointer to a function that returns an int and 
// ** requires two integer arguments
int (*f)(int a, int b);


void tester(int a, int b, int(*op)(int a, int b))
{
	cout << op(a, b) << endl;
}

int main()
{
	PLAYER ProfD = { 1 };
	int scores[10] = { 1,2,3,4,5,6,7,8,9,10 };
	DWORD dw = 0xAABBCCDD;

	// function pointer as an input arguement
	cout << "3 - 4 = ";
	tester(3, 4, sub);

	// function pointer to set operation
	f = add;
	cout << "The sum of 3 + 4 = ";
	cout << f(3, 4) << endl;


	cout << "10 + 6";
	addWithReport(10, 6, done);

	cout << "DWORD " << hex << dw << " as a low order byte ";
	cout << hex << AS_BYTE(dw) << endl;
	cout << dec;

	getPlayerScores(ProfD.id, scores, 10);
	system("pause");
	return 0;

}