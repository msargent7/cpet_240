// Professor Dellafelice
// Shown to me by Professor Gitlitz
// public private class variable

#include <iostream>
using namespace std;

class myClass
{
private: 
		int x;
	
public:
	myClass()
	{
		x = 12;
	}

	int getVal()
	{
		return x;
	}
};

int main()
{
	int* val;
	myClass *samp = new myClass();

	cout << "C++ private class variable safety - NOT!!\n\n";
	cout << "My class, myClass, has a private int x that should be hidden\n";
	cout << "Its value is: ";

	val = (int *)samp;
	cout << (*val) << endl;

	system("pause");

	return 0;
}

/*
C++ private class variable safety - NOT!!

My class, myClass, has a private int x that should be hidden
Its value is: 12
Press any key to continue . . .

*/


