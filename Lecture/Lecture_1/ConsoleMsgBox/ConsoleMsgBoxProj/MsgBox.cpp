// Professor Dellafelice
// A MessageBox from a console program?
// Just a SMOP - a Small Matter Of Programming
//
#include <iostream>
#include <windows.h>

using namespace std;

int main()
{
	int retVal;

	cout << "In a far off star system a Message Box launches...\n";

	retVal = MessageBox(NULL, TEXT("This is a Message Box\nClick OK"),
		TEXT("Message Box"), MB_OKCANCEL);

	switch (retVal)
	{
	case IDCANCEL:
		cout << "CANCEL was selected\n";
		break;
	case IDOK:
		cout << "OK was clicked\n";
		break;
	default:
		cout << "What happened here?\n";
		break;
	}

	system("pause");
	return 0;
}