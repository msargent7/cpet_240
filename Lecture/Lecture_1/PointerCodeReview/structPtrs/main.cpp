#include <iostream>
using namespace std;

// type define int as INT
typedef int INT; 

typedef struct _DATA_ {
	int a;
	int b; 
}DATA, *PDATA, **PPDATA; 

void f(PDATA ptr); 
void f1(PPDATA pptr);

int main()
{
	DATA d = { 1, 2 }; 
	PDATA pd;

	f(&d);

	f1(&pd);

	cout << pd->a << endl;

	cout << d.a << endl; 

	system("pause");

	return 0; 
}

void f(PDATA ptr)
{
	ptr->a = 999; 
}

void f1(PPDATA pptr)
{
	*pptr = new DATA;
	(**pptr).a = 12;
	(**pptr).b = 15;
}