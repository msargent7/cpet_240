#include <iostream>
using namespace std;

// Note need to pass in address of pointer variable
// so can modify its contents otherwise this 
// doesn't work!!
void getMem(int **ipp, int sz)
{
	*ipp = new int[sz];
	for (int i=0; i < sz; i++)
	{
		(*ipp)[i] = i;
	}
}

void freeMem(int *ip)
{
	delete [] ip;
}

int main()
{
	int *iPtr = NULL;

	getMem(&iPtr, 12);

	cout << iPtr[5] << endl;

	freeMem(iPtr);

	system("pause");
	return 0;
}