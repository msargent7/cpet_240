// Professor Dellafelice sample pointer code

#include <iostream>
using namespace std;

int main()
{
	int myInt = 35;									// myInt is an int						
	int *myIntAddress = &myInt;						// myIntAddress is a pointer to an int
	int **addressOfmyIntAddress = &myIntAddress;	// addressOfmyIntAddress is a pointer to a pointer to an int

	cout << "*** myInt in an integer.  It's value is: " << myInt << endl;
	cout << "*** myIntAddress is a pointer to an integer.  This means it is of type\n"
		<< "    memory address of an integer.  It's value is the memory address of myInt\n"
		<< "    which is " << (int)myIntAddress << " as a base 10 number\n";
	cout << "*** addressOfmyIntAddress is a pointer to an integer pointer.  This means\n"
		<< "    it is of type memory address of an integer pointer.  It's value is the\n"
		<< "    memory address of myIntAddress which is " << (int)addressOfmyIntAddress
		<< " as a base 10 number\n";
	cout << "*** The memory address of the variable addressOfmyIntAddress is\n"
		<< "    " << (int)(&addressOfmyIntAddress) << " as a base 10 number\n******\n******\n";

	
	cout << "*** The contents of myIntAddress is the data stored at the the memory address\n"
		<< "    of the variable myInt.  This is the same as the value of myInt which is: "
		<< *myIntAddress << endl;
	cout << "*** The contents of the value stored in addressOfmyIntAddress is the\n"
		<< "    contents of myIntAddress's memory address.  This value is the memory\n"
		<< "    address of myInt which is: " << (int)*addressOfmyIntAddress << " as a base 10 number\n";
	cout << "*** The contents of addressOfmyIntAddress is myIntAddress.  The contents of\n"
		<< "    myIntAddress is myInt.  Another way to say this is the contents of\n"
		<< "    the contents of addressOfmyIntAddress which is: " << **addressOfmyIntAddress << endl;

	system("pause");
	return 0;

}
/*
*** myInt in an integer.  It's value is: 35
*** myIntAddress is a pointer to an integer.  This means it is of type
    memory address of an integer.  It's value is the memory address of myInt
    which is 5306672 as a base 10 number
*** addressOfmyIntAddress is a pointer to an integer pointer.  This means
    it is of type memory address of an integer pointer.  It's value is the
    memory address of myIntAddress which is 5306660 as a base 10 number
*** The memory address of the variable addressOfmyIntAddress is
    5306648 as a base 10 number
******
******
*** The contents of myIntAddress is the data stored at the the memory address
    of the variable myInt.  This is the same as the value of myInt which is: 35
*** The contents of the value stored in addressOfmyIntAddress is the
    contents of myIntAddress's memory address.  This value is the memory
    address of myInt which is: 5306672 as a base 10 number
*** The contents of addressOfmyIntAddress is myIntAddress.  The contents of
    myIntAddress is myInt.  Another way to say this is the contents of
    the contents of addressOfmyIntAddress which is: 35
Press any key to continue . . .

*/
