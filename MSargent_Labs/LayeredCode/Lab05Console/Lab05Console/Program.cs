﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Repository;
using Service;
using Model;

namespace Lab05Console
{
    class Program
    {
        static List<authorViewModel> auVM;
        static authorService as1;
        static AuthorRepository ardb = new AuthorRepository();

        static void Main(string[] args)
        {
            as1 = new authorService(ardb);
            auVM = as1.getAllAuthors();
            Console.ReadKey();
        }
    }
}
