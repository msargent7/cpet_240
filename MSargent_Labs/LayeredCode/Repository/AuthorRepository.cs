﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using System.IO;
using System.Configuration;
using Model;

using System.Data.SqlClient;

namespace Repository
{
    public class returnResult
    {
        public bool success { get; set; }
        public bool message { get; set; }
    }

    public class authorGetResult : returnResult
    {
        public List<author> authors { get; set; }
    }

    public class authorSetResult : returnResult
    {
        // nothing to add 
    }

    public interface IRepository<T>
    {
        List<T> FindAll();
        T FindByID(string id);
        bool Add(T x);
        bool Update(T x);
        bool Remove(T x);
    }

    class configFile
    {
        public static string getSetting(string key)
        {
            // adapted from: 
            // https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
            // see app.config file for how to add the setting

            string setting = "";

            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                setting = appSettings[key] ?? "Not Found"; // if appSettings[key] != null, then appSettings[key], else "Not found"
            }
            catch (ConfigurationErrorsException)
            {
                setting = "ERROR: Not found";
            }

            return setting;
        }
    }


    public class AuthorRepository : IRepository<author>
    {
        private string _connectionString;

        public AuthorRepository()
        {
            _connectionString = configFile.getSetting("connectionString");
        }

        public author FindByID(string id)
        {
            return default(author);

        }

        public bool Add(author au)
        {
            return false;
        }

        public bool Update(author au)
        {
            return false;
        }

        public bool Remove(author au)
        {
            return false;
        }

        public List<author> FindAll()
        {
            List<author> authors = new List<author>();


            string file_path = "";

            try
            {
                file_path = configFile.getSetting("authorFile");
            }
            catch (Exception ex)
            {
                Console.Write("oops");
            }

            int count = 0;
            int[] widths = null;

            using (StreamReader reader = File.OpenText(file_path))
            {
                string line = null;


                line = reader.ReadLine(); // get line from file, or null at end of file

                while (line != null)
                {
                    if (count == 0) // first row
                    {
                        count++;

                    }
                    else if (count == 1) // second row 
                    {
                        string[] fields = line.Split(null as string[], StringSplitOptions.RemoveEmptyEntries);
                        widths = new int[fields.Count()];

                        for (int i = 0; i < fields.Count(); i++)
                        {
                            widths[i] = fields[i].Length;
                        }

                        count++;
                    }
                    else if (char.IsNumber(line[0]))
                    {
                        List<string> fields = new List<string>();

                        for (int i = 0; i < widths.Count(); i++)
                        {
                            int width = widths[i];
                            char[] a = new char[width];

                            line = line.Trim();

                            using (StringReader sr = new StringReader(line))
                            {
                                sr.Read(a, 0, width);
                                fields.Add(new string(a).Trim());
                            }

                            if (i < widths.Count() - 1)
                                line = line.Remove(0, width);
                        }

                        //ListViewItem lvi = null;

                        Boolean contract = false;

                        if (fields[8][0] == '1') contract = true;

                        author au = new author(fields[0], fields[2], fields[1], fields[3], fields[4], fields[5], fields[6], fields[7], contract);

                        authors.Add(au);
                    }

                    line = reader.ReadLine(); // next line in file 
                }
            }

            return authors;
        }
    }


    public class authorDAL
    {
        private string _file;
        XmlDocument _doc;
        XmlNode _node;


        public authorDAL(string file)
        {
            _file = file;

            _doc = new XmlDocument();

            _doc.Load(_file);
        }

        public string file
        {
            get { return _file; }
        }

        // get
        XmlNode find(string id)
        {
            return null;
        }

        public bool create(string id, string lname, string fname, string phone,
                           string address, string city, string state, string zip, string contract)
        {
            // http://navinpandit.blogspot.in/2016/12/exception-this-document-already-has.html
            XmlNode node = _doc.CreateElement("author");
            XmlNode contact = _doc.CreateElement("contact");
            XmlAttribute au_id = _doc.CreateAttribute("au_id");

            au_id.InnerText = id;
            node.Attributes.Append(au_id);

            node.AppendChild(_doc.CreateElement("au_lname"));
            node["au_lname"].InnerText = lname;

            node.AppendChild(_doc.CreateElement("au_fname"));
            node["au_fname"].InnerText = fname;

            contact.AppendChild(_doc.CreateElement("phone"));
            contact["phone"].InnerText = phone;

            contact.AppendChild(_doc.CreateElement("address"));
            contact["address"].InnerText = address;

            contact.AppendChild(_doc.CreateElement("city"));
            contact["city"].InnerText = city;

            contact.AppendChild(_doc.CreateElement("state"));
            contact["state"].InnerText = state;

            contact.AppendChild(_doc.CreateElement("zip"));
            contact["zip"].InnerText = zip;

            node.AppendChild(contact);

            node.AppendChild(_doc.CreateElement("contract"));
            node["contract"].InnerText = contract;

            XmlNode root = _doc.SelectSingleNode("authors");

            root.AppendChild(node);

            _doc.Save(_file);

            return false;
        }

        // remove
        public bool delete(string id)
        {
            XmlNode nodeToDelete = _doc.SelectSingleNode(string.Format("authors/author[@au_id=\"{0}\"]", id));
            if (nodeToDelete != null)
            {
                nodeToDelete.ParentNode.RemoveChild(nodeToDelete);
            }
            _doc.Save(_file);

            return false;
        }

        // update
        public bool update(string id, string oldInfo, string newInfo)
        {
            XmlNode nodes = _doc.GetElementsByTagName("au_id")[0].ChildNodes[0];
            nodes.InnerText = "newInfo";
            _doc.Save(_file);

            return false;
        }
    }
}
