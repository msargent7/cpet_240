﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;

namespace xmlLab
{
    public class DataReader
    {
        /// <summary>
        /// Reads the XML file specified in the config and returns the root node
        /// </summary>
        /// <returns>The root node, or null if the file wasn't found or a valid XML file</returns>
        public static XmlElement ReadXmlData()
        {
            string path = ConfigurationManager.AppSettings.Get("filepath");
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            }
            catch (System.IO.FileNotFoundException)
            {
                return null;
            }

            return doc.DocumentElement;
        }
    }
}
