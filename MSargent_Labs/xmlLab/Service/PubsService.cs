﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Model;
using Repository; 

namespace Service
{
    public class authorService
    {
        IRepository<author> _repository;

        public authorService(IRepository<author> repository)
        {
            _repository = repository; 
        }

        public List<authorViewModel> getAllAuthors()
        {
            List<authorViewModel> authors = new List<authorViewModel>();

            List<author> auList = _repository.FindAll(); 

            foreach(author au in auList)
            {
                authors.Add(viewModelFromAuthor(au)); 
            }

            return authors; 
        }

        public bool updateAuthor(author au)
        {
            // or pass in author view model and convert to author
            // should the UI layer be exposed to authors or author view model only ?????
            if(_repository.Update(au) == true)
            {
                return true; 
            }

            return false;
        }

        public bool addAuthor(authorViewModel auVM)
        {
            author au = VMToAuthor(auVM);
            if(_repository.FindByID(au.id) == null)
            {
                return _repository.Add(au); 
            }

            return false; 
        }

        public bool removeAuthor(authorViewModel auVM)
        {
            author au = VMToAuthor(auVM);
            return _repository.Remove(au);
            
        }

        public authorViewModel getAuthorByID(string au_id)
        {
            author au = _repository.FindByID(au_id);
            if(au == null)
            {
                return null;
            }
            return viewModelFromAuthor(au);
        }

        public authorViewModel viewModelFromAuthor(author au)
        {
            authorViewModel a = new authorViewModel(au.id);

            a.FirstName = au.fname;
            a.LastName = au.lname;
            a.Phone = au.phone;
            a.Zip = au.zip;
            a.State = au.state;
            a.City = au.city;
            a.Address = au.address; 

            if(au.contract) a.Contract = "Yes";
            else a.Contract = "No"; 
            
            return a; 
        }

        public author VMToAuthor(authorViewModel auvm)
        {
            Boolean contract = (auvm.Contract == "Yes");
            return new author(auvm.ID, auvm.FirstName, auvm.LastName, auvm.Phone,
                                auvm.Address, auvm.City,auvm.State, auvm.Zip, contract);
        }

    }

    public class authorViewModel
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ID { get; private set; }
        public string Name { get { return FirstName + " " + LastName; } }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Contract { get; set; }

        public authorViewModel(string id)
        {
            ID = id;
        }

        public authorViewModel(string id, string fname, string lname, string phone, string address,
                              string city, string state, string zip, string contract)
        {
            FirstName = fname;
            LastName = lname;
            ID = id;
            Phone = phone;
            Address = address;
            City = city;
            State = state;
            Zip = zip;
            Contract = contract;
        }


    }
}
