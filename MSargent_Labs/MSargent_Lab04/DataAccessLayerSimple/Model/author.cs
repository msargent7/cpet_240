﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class author
    {
        private string _id;
        private string _fname;
        private string _lname;
        private string _phone;
        private string _address;
        private string _city;
        private string _state;
        private string _zip;
        private string _contract;

        public author(string au_id, string fname, string lname, string phone, string address,
                      string city, string state, string zip, string contract)
        {
            _id = au_id;
            _fname = fname;
            _lname = lname;
            _phone = phone;
            _address = address;
            _city = city;
            _state = state;
            _zip = zip;
            _contract = contract;
        }

        public string id
        {
            get { return _id; } // read only
        }

        public string fname
        {
            get { return _fname; }
            set { _fname = value; }
        }

        public string lname
        {
            get { return _lname; }
            set { _lname = value; }
        }

        public string name
        {
            get { return _fname + " " + _lname; }
        }

        public string phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public string address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string city
        {
            get { return _city; }
            set { _city = value; }
        }

        public string state
        {
            get { return _state; }
            set { _state = value; }
        }

        public string zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        public string contract
        {
            get { return _contract; }
            set { _contract = value; }
        }
    }
}
