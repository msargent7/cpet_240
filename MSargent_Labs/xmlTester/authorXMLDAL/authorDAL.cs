﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;

namespace xmlAuthorDal
{
    public class authorDAL
    {
        private string _file;
        XmlDocument _doc;
        XmlNode _node;


        public authorDAL(string file)
        {
            _file = file;

            _doc = new XmlDocument();

            _doc.Load(_file);
        }

        public string file
        {
            get { return _file; }
        }

        // get
        XmlNode find(string id)
        {
            return null;
        }

        public bool create(string id, string lname, string state)
        {
            // http://navinpandit.blogspot.in/2016/12/exception-this-document-already-has.html
            XmlNode node = _doc.CreateElement("author");
            XmlNode contact = _doc.CreateElement("contact");
            XmlAttribute au_id = _doc.CreateAttribute("au_id");

            au_id.InnerText = id;
            node.Attributes.Append(au_id);

            node.AppendChild(_doc.CreateElement("au_lname"));
            node["au_lname"].InnerText = lname;

            contact.AppendChild(_doc.CreateElement("state"));
            contact["state"].InnerText = "NH";

            node.AppendChild(contact);

            XmlNode root = _doc.SelectSingleNode("authors");

            root.AppendChild(node);

            _doc.Save(_file);

            return false;
        }

        // remove
        public bool remove(string id, string state)
        {
            XmlNode nodeToDelete = _doc.SelectSingleNode(string.Format("authors/author[@au_id=\"{0}\"]", id));
            if (nodeToDelete != null)
            {
                nodeToDelete.ParentNode.RemoveChild(nodeToDelete);
            }
            _doc.Save(_file);

            return false;
        }

        // update
        public bool update(string id, string oldInfo, string newInfo)
        {

            foreach (XmlElement element in _doc.SelectSingleNode(string.Format("authors/author[@au_id=\"{0}\"]", id)))
            {
                foreach (XmlElement element1 in element)
                {
                    if (element.SelectSingleNode("au_lname").InnerText == oldInfo)
                    {
                        XmlNode newvalue = _doc.CreateElement("au_lname");
                        newvalue.InnerText = newInfo;
                        element.ReplaceChild(newvalue, element1);

                        _doc.Save(_file);
                    }
                }
            }
                _doc.Save(_file);

                return false;
            } 
        }
    }
