﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using xmlAuthorDal;

namespace xmlTester
{
    class Program
    {
        static void Main(string[] args)
        {
            xmlAuthorDal.authorDAL dal = new xmlAuthorDal.authorDAL("authors.xml");

            Console.WriteLine(dal.file);

            // This adds a reduced node
            //dal.create("123-45-6789", "Sargent", "NH");
            //dal.remove("899-46-2035", "UT");
            dal.update("899-46-2035", "Ringer", "Smith");
        }
    }
}
