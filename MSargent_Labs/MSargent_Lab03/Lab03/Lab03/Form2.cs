﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Forms;

namespace Lab03
{
    public partial class Form2 : Form
    {
        private Form1 myForm1;
        public Form2(Form1 form1)
        {
            myForm1 = form1;
            InitializeComponent();
        }

        Form1.WageInfo info = new Form1.WageInfo();


        //$8.75
        private void radioButton1_Click(object sender, EventArgs e)
        {
            textBox2.Enabled = false;
            textBox2.ReadOnly = true;
            textBox2.Text = null;
        }

        //$7.33
        private void radioButton2_Click(object sender, EventArgs e)
        {
            textBox2.Enabled = false;
            textBox2.ReadOnly = true;
            textBox2.Text = null;
        }
        //$10.00
        private void radioButton3_Click(object sender, EventArgs e)
        {
            textBox2.Enabled = false;
            textBox2.ReadOnly = true;
            textBox2.Text = null;
        }

        //$11.20
        private void radioButton4_Click(object sender, EventArgs e)
        {
            textBox2.Enabled = false;
            textBox2.ReadOnly = true;
            textBox2.Text = null;
        }

        //Other Non Wage
        private void radioButton5_Click(object sender, EventArgs e)
        {
            textBox2.Enabled = true;
            textBox2.ReadOnly = false;
        }

        //Non Std Wage Rate
        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only numbers and proper punctiuation to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsNumber(pressedKey) ||
                    Char.IsControl(pressedKey) ||
                    e.KeyChar == '.' ||
                    e.KeyChar == ',')
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        //Hours Worked
        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only numbers and proper punctiuation to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsNumber(pressedKey) ||
                    Char.IsControl(pressedKey) ||
                    e.KeyChar == '.' ||
                    e.KeyChar == ',')
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        //Full Name
        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only letters and proper punction to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsLetter(pressedKey) ||
                    Char.IsWhiteSpace(pressedKey) ||
                    e.KeyChar == '\'' ||
                    e.KeyChar == '.')
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        //Run Calculations
        private void button1_Click(object sender, EventArgs e)
        {
            double wage;
            double hours;
            string name;

            //Check hours worked
            try
            {
                double.Parse(textBox4.Text);
            }
            catch (Exception)
            {
                SystemSounds.Beep.Play();
                MessageBox.Show("Please enter a valid number for your hours");
            }

            //Check full name
            if (string.IsNullOrWhiteSpace(textBox6.Text.Trim()))
            {
                SystemSounds.Beep.Play();
                MessageBox.Show("Please enter a valid name");
                return;
            }

            else
            {

                //Set wages
                if (radioButton1.Checked)
                {
                    wage = 8.75;
                    hours = Convert.ToDouble(textBox4.Text.Trim());
                    name = textBox6.Text.Trim();
                    info.setInfo(wage, hours, name);
                }
                else if (radioButton2.Checked)
                {
                    wage = 9.33;
                    hours = Convert.ToDouble(textBox4.Text.Trim());
                    name = textBox6.Text.Trim();
                    info.setInfo(wage, hours, name);
                }
                else if (radioButton3.Checked)
                {
                    wage = 10.00;
                    hours = Convert.ToDouble(textBox4.Text.Trim());
                    name = textBox6.Text.Trim();
                    info.setInfo(wage, hours, name);
                }
                else if (radioButton4.Checked)
                {
                    wage = 11.20;
                    hours = Convert.ToDouble(textBox4.Text.Trim());
                    name = textBox6.Text.Trim();
                    info.setInfo(wage, hours, name);
                }
                //Check for other wage radio button
                //If selected, check to ensure hours are entered in proper format
                //Read in data from text boxes and set wage info
                else if (radioButton5.Checked)
                {

                    if (!double.TryParse(textBox2.Text, out wage))
                    {
                        MessageBox.Show("Please enter a valid number for your wage");
                        return;
                    }
                    else
                    {
                        wage = Convert.ToDouble(textBox2.Text.Trim());
                        hours = Convert.ToDouble(textBox4.Text.Trim());
                        name = textBox6.Text.Trim();
                        info.setInfo(wage, hours, name);
                    }
                }
            }

            //Send the wage info back to Form1
            myForm1.Form1_Text_Setter(info);
            //Refresh Form1 to show both new calculated wages and previously calculated
            myForm1.Refresh();

        }

        //Hide the form so it can be used again when the window is suspended
        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
