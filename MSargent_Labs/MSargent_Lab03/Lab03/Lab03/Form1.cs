﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public class WageInfo : EventArgs
        {
            public double wage;
            public double hours;
            public string name;

            public void setInfo(double w, double h, string n)
            {
                this.wage = w;
                this.hours = h;
                this.name = n;
            }
        }

        public WageInfo _info { get; set; }

        private static Form2 inputData = null;

        private void inputsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Check to see if the form already exists
            //Create a new form if one does not exist
            //Else show existing form
            if (inputData == null)
            {
                inputData = new Form2(this);
            }
            inputData.ShowDialog();
        }

        public void Form1_Text_Setter(WageInfo info)
        {
            //Set the variable for calculating the pay information
            //Calculated using the values contained in the info class
            double grossPay;
            double netPay;
            double tax;
            const double taxRate = 0.15;

            grossPay = info.wage * info.hours;
            tax = grossPay * taxRate;
            netPay = grossPay - tax;

            //Output the wage information in the correct format
            string s = $"Name: \t\t{info.name}\r\n" +
                       $"Hours Worked: \t{info.hours}\r\n" +
                       $"Wage rate: \t{info.wage:C}\r\n" +
                       $"Gross wage: \t{grossPay:C}\r\n" +
                       $"Taxes: \t\t{tax:C}\r\n" +
                       $"Net wages: \t{netPay:C}\r\n";

            //Check to see if the string is empty
            //If it is not, create a new line and add the new data below existing
            //Else add to the top of the text box
            if (!string.IsNullOrEmpty(resultsBox.Text))
            {
                resultsBox.AppendText(Environment.NewLine);
            }
            resultsBox.AppendText(s);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void notYetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not done yet!");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Project created by\n" +
                "Michael Sargent\n" +
                "Current Date and Time: " + DateTime.Now.ToString());
        }

    }
}
