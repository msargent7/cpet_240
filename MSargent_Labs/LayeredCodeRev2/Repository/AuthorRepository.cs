﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using System.IO;
using System.Configuration;
using Model;

using System.Data.SqlClient;

namespace Repository
{
    public class returnResult
    {
        public bool success { get; set; }
        public bool message { get; set; }
    }

    public class authorGetResult : returnResult
    {
        public List<author> authors { get; set; }
    }

    public class authorSetResult : returnResult
    {
        // nothing to add 
    }

    public interface IRepository<T>
    {
        List<T> FindAll();
        T FindByID(string id);
        bool Add(T x);
        bool Update(T x);
        bool Remove(T x);
    }

    public class configFile
    {
        public static string getSetting(string key)
        {
            // adapted from: 
            // https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
            // see app.config file for how to add the setting

            string setting = "";

            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                setting = appSettings[key] ?? "Not Found"; // if appSettings[key] != null, then appSettings[key], else "Not found"
            }
            catch (ConfigurationErrorsException)
            {
                setting = "ERROR: Not found";
            }

            return setting;
        }
    }

    public class AuthorRepositoryXML : IRepository<author>
    {
        public author FindByID(string id)
        {
            List<author> authors = FindAll();

            foreach (author au in authors)
            {
                if (au.id == id) return au;
            }

            return default(author);

        }

        public bool Add(author au)
        {
            XmlDocument _doc = new XmlDocument();
            _doc.Load("authors.xml");

            XmlNode node = _doc.CreateElement("author");
            XmlNode contact = _doc.CreateElement("contact");
            XmlAttribute au_id = _doc.CreateAttribute("au_id");

            au_id.InnerText = au.id;
            node.Attributes.Append(au_id);

            node.AppendChild(_doc.CreateElement("au_lname"));
            node["au_lname"].InnerText = au.lname;

            node.AppendChild(_doc.CreateElement("au_fname"));
            node["au_fname"].InnerText = au.fname;

            contact.AppendChild(_doc.CreateElement("phone"));
            contact["phone"].InnerText = au.phone;

            contact.AppendChild(_doc.CreateElement("address"));
            contact["address"].InnerText = au.address;

            contact.AppendChild(_doc.CreateElement("city"));
            contact["city"].InnerText = au.city;

            contact.AppendChild(_doc.CreateElement("state"));
            contact["state"].InnerText = au.state;

            contact.AppendChild(_doc.CreateElement("zip"));
            contact["zip"].InnerText = au.zip;

            node.AppendChild(_doc.CreateElement("contract"));
            node["contract"].InnerText = au.contract.ToString();


            node.AppendChild(contact);

            XmlNode root = _doc.SelectSingleNode("authors");

            root.AppendChild(node);

            _doc.Save("authors.xml");


            return true;
        }

        public bool Update(author au)
        {
            if (Remove(au))
            {
                if (Add(au)) return true;
                return true;
            }

            return false;
        }

        public bool Remove(author au)
        {
            XmlDocument _doc = new XmlDocument();

            _doc.Load("authors.xml");

            XmlNode nodeToDelete = _doc.SelectSingleNode(string.Format("authors/author[@au_id=\"{0}\"]", au.id));
            if (nodeToDelete != null)
            {
                nodeToDelete.ParentNode.RemoveChild(nodeToDelete);
            }
            _doc.Save("authors.xml");

            return false;
        }

        public List<author> FindAll()
        {
            List<author> authors = new List<author>();

            // @ means ignore escape sequences
            using (XmlReader rdr = XmlReader.Create("authors.xml"))
            {
                while (rdr.Read())
                {
                    if (rdr.IsStartElement())
                    {
                        if (rdr.Name == "author")
                        {

                            author au = new author();

                            au.id = rdr.GetAttribute("au_id");

                            rdr.Read();
                            
                            while (rdr.Name != "author")
                            {
                                if (rdr.NodeType == XmlNodeType.Element)
                                {
                                    switch (rdr.Name)
                                    {
                                        case "au_lname":
                                            au.lname = rdr.ReadInnerXml();
                                            break;
                                        case "au_fname":
                                            au.fname = rdr.ReadInnerXml();
                                            break;
                                        case "phone":
                                            au.phone = rdr.ReadInnerXml();
                                            break;
                                        case "address":
                                            au.address = rdr.ReadInnerXml();
                                            break;
                                        case "city":
                                            au.city = rdr.ReadInnerXml();
                                            break;
                                        case "state":
                                            au.state = rdr.ReadInnerXml();
                                            break;
                                        case "zip":
                                            au.zip = rdr.ReadInnerXml();
                                            break;
                                        case "contract":
                                            au.contract = false;
                                            if (rdr.ReadInnerXml() == "1") au.contract = true;
                                            break;
                                        default:
                                            rdr.Read();
                                            break;
                                    }
                                }
                                else
                                {
                                    rdr.Read();
                                }
                            }
                            authors.Add(au);
                        }
                    }
                }
            }            
            return authors;
        }
    }
}
