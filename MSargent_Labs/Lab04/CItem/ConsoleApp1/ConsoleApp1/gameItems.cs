﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameItems
{
    abstract class CItem
    {
        private int _strength;

        public CItem()
        {
            _strength = 2;
        }

        public CItem(int strength)
        {
            Console.WriteLine("CItem constructor with arg");
            _strength = strength;
        }

        public int strength
        {
            get { return _strength; }
        }

        abstract public void use();
    }

    //Allow use of abstract class
    class spell : CItem
    {
        public spell()
        {
            Console.WriteLine("Spell constructor");
        }
        public spell(int stg) : base(stg)
        {
            Console.WriteLine("In spell constructor");
        }
        public override void use()
        {
            Console.WriteLine("Cast a spell");
        }
    }

    class potion : CItem
    {
        public potion() : base(20)
        {
            Console.WriteLine("Potion constructor");
            Console.WriteLine("strength is: {0}", strength);
        }
        public override void use()
        {
            Console.WriteLine("Potion city");
        }
        public override string ToString()
        {
            return ("I am abc");
        }
        public void doThis()
        {
            Console.WriteLine("Do this");
        }
    }
}
