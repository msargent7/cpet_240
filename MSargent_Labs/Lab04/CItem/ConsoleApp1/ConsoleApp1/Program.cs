﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gameItems;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            CItem item1 = new spell();
            CItem item2 = new potion();
            potion item3 = new potion();
            item1.use();
            Console.WriteLine(item1.ToString());
            Console.WriteLine(item2.ToString());
            item2.use();
            //Child has the power to doThis - not parent
            //item2.doThis();
            item3.doThis();

            if (item3 is potion)
            {
                Console.WriteLine("Item 3 is a potion");
            }

            if (item2 is potion)
            {
                Console.WriteLine("Item 2 is a potion");
            }

            Console.ReadKey();
        }
    }
}
