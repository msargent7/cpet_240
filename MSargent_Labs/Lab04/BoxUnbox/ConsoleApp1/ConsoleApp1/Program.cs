﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Boxing
            int i = 42;
            object o = i;
            i = 3;
            Console.WriteLine("{0},{1}", i, o);

            //Unboxing
            object z = 42;
            int k = (int)z;
            Console.WriteLine("{0},{1}", z, k);
            Console.ReadKey();
        }
    }
}
