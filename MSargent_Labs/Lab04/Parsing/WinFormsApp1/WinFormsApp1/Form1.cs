﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = String.Format("{0:C}", 123.569);
            try
            {
                if (!String.IsNullOrWhiteSpace(textBox1.Text))
                {
                    throw new Exception("What");
                }
            }
            catch(Exception e1)
            {
                textBox1.Text = "Uh-oh!" + e1.Message;
            }
        }

        private void PushClick(object sender, EventArgs e)
        {
            textBox1.Text = "123.45";
            double val;
            if(double.TryParse(textBox1.Text, out val))
            {
                MessageBox.Show("the value is: " + val);
            }

            try
            {
                textBox1.Text = "            abc def";
                textBox1.Text = textBox1.Text.Trim();
                double.Parse(textBox1.Text);
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                MessageBox.Show("That's all folks");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsLetter(pressedKey))
            {
                e.Handled = false;
            }
        }
    }
}
