﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void inputsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 inputData = new Form2();
            inputData.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void notYetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not done yet!");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Project created by\n" +
                "Michael Sargent\n" +
                "Current Date and Time: " + DateTime.Now.ToString());
        }

    }
}
