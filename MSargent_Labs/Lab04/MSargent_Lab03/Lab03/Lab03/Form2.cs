﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab03
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        double totalPay;
        double tax;
        double netPay;

        public class WageInfo : EventArgs
        {
            public double wage;
            public double hours;
            public string name;

            public void setInfo(double w, double h, string n)
            {
                this.wage = w;
                this.hours = h;
                this.name = n;
            }
        }

        //$8.75
        private void radioButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Button 1 Clicked!");
            nWageBox.Enabled = false;
            nWageBox.ReadOnly = true;
            nWageBox.Text = null;
        }

        //$9.33
        private void radioButton2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Button 2 Clicked!");
            nWageBox.Enabled = false;
            nWageBox.ReadOnly = true;
            nWageBox.Text = null;
        }
        //$10.00
        private void radioButton3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Button 3 Clicked!");
            nWageBox.Enabled = false;
            nWageBox.ReadOnly = true;
            nWageBox.Text = null;
        }

        //$11.20
        private void radioButton4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Button 4 Clicked!");
            nWageBox.Enabled = false;
            nWageBox.ReadOnly = true;
            nWageBox.Text = null;
        }

        //Other Non Wage
        private void radioButton5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Button 5 Clicked!");
            nWageBox.Enabled = true;
            nWageBox.ReadOnly = false;
            nWageBox.Text = null;
        }

        //Non Std Wage Rate
        private void nWageBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only numbers and proper punctiuation to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsNumber(pressedKey) ||
                    Char.IsControl(pressedKey) ||
                    e.KeyChar == '.')
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }
        private void nWageBox_TextChanged(object sender, EventArgs e)
        {
            TextBox t2 = (TextBox)sender;
            string nWage = t2.Text.Trim();
        }

        //Hours Worked
        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only numbers and proper punctiuation to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsNumber(pressedKey) || 
                    Char.IsControl(pressedKey) ||
                    e.KeyChar == '.' ||
                    e.KeyChar == ',')
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            TextBox t4 = (TextBox)sender;
            string hours = t4.Text.Trim();
        }

        //Full Name
        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only letters and proper punction to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsLetter(pressedKey) ||
                    Char.IsWhiteSpace(pressedKey) ||
                    e.KeyChar == '\''||
                    e.KeyChar == '.')
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }
        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            TextBox t6 = (TextBox)sender;
            string name = t6.Text.Trim();
        }

        //Run Calculations
        private void button1_Click(object sender, EventArgs e)
        {


        }
    }
}

