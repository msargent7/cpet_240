﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace firstCS
{
    class person
    {
        private string m_fname;
        private string m_lname;
        private int m_age = 12;

        public person(string FirstName, string  LastName, int age)
        {
            m_fname = FirstName;
            m_lname = LastName;
            m_age = age;
        }

        public int age
        {
            set { m_age = value; }
            get { return m_age; }
            //get { return m_age; }
        }

        public string name
        {
            set { m_fname = "Sam"; m_lname = "Flam"; }
            get { return string.Format("{0} {1}", m_fname, m_lname); }
        }

        public override string ToString()
        {
            string all = string.Format("Name: {0}, Age: {1}", this.name, this.age);
            return all;
        }



    }
}
