﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using firstCS;

namespace CSharpExample1
{
    class Program
    {
        static string msg = "Hello World";
        static void Main(string[] args)
        {
            List<person> people = new List<person>();

            person p1 = new person("Sue", "Flue", 22);
            person p2 = new person("Sam", "Jam", 18);

            people.Add(p1);
            people.Add(p2);

            person p = null;
            p = new person("Gary", "Scary", 85);

            people.Add(p);

            foreach(person pr in people)
            {
                Console.WriteLine(pr);
            }

            p1 = p2;
            Console.WriteLine(string.Format("{0}", p1.age));
            p1.age = 15;
            Console.WriteLine(string.Format("{0:C}", p1.age));
            Console.WriteLine(string.Format("{0}", p2.name));
            Console.WriteLine(string.Format("{0}", p1));
            //Console.WriteLine(msg);
            int y = 10;
            func(ref y);
            Console.WriteLine(y);
            func2(ref p1);
            Console.WriteLine(p1.name);
            Console.ReadKey();
        }

        static void func(ref int x)
        {
            x = 15;
        }

        static void func2(ref person p3)
        {
            p3 = new person("Joe", "Toe", 54);
        }

    }
}
