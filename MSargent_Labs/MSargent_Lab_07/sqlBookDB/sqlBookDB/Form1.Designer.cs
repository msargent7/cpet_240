﻿namespace sqlBookDB
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvAuthors = new System.Windows.Forms.ListView();
            this.lvAuthorBooks = new System.Windows.Forms.ListView();
            this.lvAllBooks = new System.Windows.Forms.ListView();
            this.addBookButton = new System.Windows.Forms.Button();
            this.bookToAuButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.titleBox = new System.Windows.Forms.TextBox();
            this.typeBox = new System.Windows.Forms.TextBox();
            this.pubIdBox = new System.Windows.Forms.TextBox();
            this.priceBox = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.saveButton = new System.Windows.Forms.Button();
            this.quitButton = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.titleIDBox = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.removeBookButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvAuthors
            // 
            this.lvAuthors.HideSelection = false;
            this.lvAuthors.Location = new System.Drawing.Point(28, 47);
            this.lvAuthors.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvAuthors.Name = "lvAuthors";
            this.lvAuthors.Size = new System.Drawing.Size(433, 322);
            this.lvAuthors.TabIndex = 67;
            this.lvAuthors.UseCompatibleStateImageBehavior = false;
            this.lvAuthors.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvAuthors_MouseClick);
            // 
            // lvAuthorBooks
            // 
            this.lvAuthorBooks.HideSelection = false;
            this.lvAuthorBooks.Location = new System.Drawing.Point(28, 421);
            this.lvAuthorBooks.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvAuthorBooks.Name = "lvAuthorBooks";
            this.lvAuthorBooks.Size = new System.Drawing.Size(847, 158);
            this.lvAuthorBooks.TabIndex = 435;
            this.lvAuthorBooks.UseCompatibleStateImageBehavior = false;
            // 
            // lvAllBooks
            // 
            this.lvAllBooks.HideSelection = false;
            this.lvAllBooks.Location = new System.Drawing.Point(581, 47);
            this.lvAllBooks.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvAllBooks.Name = "lvAllBooks";
            this.lvAllBooks.Size = new System.Drawing.Size(433, 322);
            this.lvAllBooks.TabIndex = 54;
            this.lvAllBooks.UseCompatibleStateImageBehavior = false;
            // 
            // addBookButton
            // 
            this.addBookButton.Location = new System.Drawing.Point(1135, 439);
            this.addBookButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.addBookButton.Name = "addBookButton";
            this.addBookButton.Size = new System.Drawing.Size(181, 30);
            this.addBookButton.TabIndex = 6;
            this.addBookButton.Text = "Add New Book";
            this.addBookButton.UseVisualStyleBackColor = true;
            this.addBookButton.Click += new System.EventHandler(this.addBookButton_Click);
            // 
            // bookToAuButton
            // 
            this.bookToAuButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bookToAuButton.Location = new System.Drawing.Point(493, 106);
            this.bookToAuButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bookToAuButton.Name = "bookToAuButton";
            this.bookToAuButton.Size = new System.Drawing.Size(55, 52);
            this.bookToAuButton.TabIndex = 22;
            this.bookToAuButton.Text = "<";
            this.bookToAuButton.UseVisualStyleBackColor = true;
            this.bookToAuButton.Click += new System.EventHandler(this.bookToAuButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(471, 82);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(103, 15);
            this.textBox1.TabIndex = 20;
            this.textBox1.Text = "Add Book";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(471, 165);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(103, 15);
            this.textBox2.TabIndex = 24;
            this.textBox2.Text = "To Author";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // titleBox
            // 
            this.titleBox.Enabled = false;
            this.titleBox.Location = new System.Drawing.Point(1135, 106);
            this.titleBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.titleBox.Name = "titleBox";
            this.titleBox.ReadOnly = true;
            this.titleBox.Size = new System.Drawing.Size(180, 22);
            this.titleBox.TabIndex = 1;
            // 
            // typeBox
            // 
            this.typeBox.Enabled = false;
            this.typeBox.Location = new System.Drawing.Point(1135, 156);
            this.typeBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.typeBox.Name = "typeBox";
            this.typeBox.ReadOnly = true;
            this.typeBox.Size = new System.Drawing.Size(180, 22);
            this.typeBox.TabIndex = 2;
            // 
            // pubIdBox
            // 
            this.pubIdBox.Enabled = false;
            this.pubIdBox.Location = new System.Drawing.Point(1135, 203);
            this.pubIdBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pubIdBox.Name = "pubIdBox";
            this.pubIdBox.ReadOnly = true;
            this.pubIdBox.Size = new System.Drawing.Size(180, 22);
            this.pubIdBox.TabIndex = 3;
            // 
            // priceBox
            // 
            this.priceBox.Enabled = false;
            this.priceBox.Location = new System.Drawing.Point(1135, 250);
            this.priceBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.priceBox.Name = "priceBox";
            this.priceBox.ReadOnly = true;
            this.priceBox.Size = new System.Drawing.Size(180, 22);
            this.priceBox.TabIndex = 4;
            this.priceBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.priceBox_KeyPress);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Checked = false;
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(1135, 310);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(180, 22);
            this.dateTimePicker1.TabIndex = 5;
            // 
            // saveButton
            // 
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(1135, 491);
            this.saveButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(181, 30);
            this.saveButton.TabIndex = 7;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // quitButton
            // 
            this.quitButton.Location = new System.Drawing.Point(1135, 542);
            this.quitButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(181, 30);
            this.quitButton.TabIndex = 8;
            this.quitButton.Text = "Quit";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click);
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(1044, 66);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(83, 15);
            this.textBox3.TabIndex = 15;
            this.textBox3.Text = "Title ID:";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(1044, 114);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(83, 15);
            this.textBox4.TabIndex = 16;
            this.textBox4.Text = "Title:";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Location = new System.Drawing.Point(1044, 165);
            this.textBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(83, 15);
            this.textBox5.TabIndex = 17;
            this.textBox5.Text = "Type:";
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Location = new System.Drawing.Point(1044, 212);
            this.textBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(83, 15);
            this.textBox6.TabIndex = 18;
            this.textBox6.Text = "Pub ID:";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Location = new System.Drawing.Point(1044, 258);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(83, 15);
            this.textBox7.TabIndex = 19;
            this.textBox7.Text = "Price:";
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Location = new System.Drawing.Point(1044, 319);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(83, 15);
            this.textBox8.TabIndex = 20;
            this.textBox8.Text = "Pub Date:";
            // 
            // titleIDBox
            // 
            this.titleIDBox.Enabled = false;
            this.titleIDBox.Location = new System.Drawing.Point(1135, 58);
            this.titleIDBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.titleIDBox.Name = "titleIDBox";
            this.titleIDBox.ReadOnly = true;
            this.titleIDBox.Size = new System.Drawing.Size(180, 22);
            this.titleIDBox.TabIndex = 0;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(28, 386);
            this.textBox9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(847, 27);
            this.textBox9.TabIndex = 436;
            this.textBox9.Text = "Books By Selected Author";
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(28, 16);
            this.textBox10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(435, 23);
            this.textBox10.TabIndex = 437;
            this.textBox10.Text = "Authors";
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(581, 16);
            this.textBox11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(435, 23);
            this.textBox11.TabIndex = 438;
            this.textBox11.Text = "Books";
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // removeBookButton
            // 
            this.removeBookButton.Location = new System.Drawing.Point(883, 491);
            this.removeBookButton.Margin = new System.Windows.Forms.Padding(4);
            this.removeBookButton.Name = "removeBookButton";
            this.removeBookButton.Size = new System.Drawing.Size(181, 30);
            this.removeBookButton.TabIndex = 439;
            this.removeBookButton.Text = "Remove Selected Book";
            this.removeBookButton.UseVisualStyleBackColor = true;
            this.removeBookButton.Click += new System.EventHandler(this.removeBookButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1353, 635);
            this.Controls.Add(this.removeBookButton);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.titleIDBox);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.priceBox);
            this.Controls.Add(this.pubIdBox);
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.titleBox);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.bookToAuButton);
            this.Controls.Add(this.addBookButton);
            this.Controls.Add(this.lvAllBooks);
            this.Controls.Add(this.lvAuthorBooks);
            this.Controls.Add(this.lvAuthors);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Books by Author DB";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvAuthors;
        private System.Windows.Forms.ListView lvAuthorBooks;
        private System.Windows.Forms.ListView lvAllBooks;
        private System.Windows.Forms.Button addBookButton;
        private System.Windows.Forms.Button bookToAuButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox titleBox;
        private System.Windows.Forms.TextBox typeBox;
        private System.Windows.Forms.TextBox pubIdBox;
        private System.Windows.Forms.TextBox priceBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button quitButton;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox titleIDBox;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Button removeBookButton;
    }
}

