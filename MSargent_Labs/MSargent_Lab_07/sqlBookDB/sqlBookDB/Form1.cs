﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Model;
using Repository;
using Service;
using System.Text.RegularExpressions;

namespace sqlBookDB
{
    public partial class Form1 : Form
    {
        private bool newButtonClicked = false;
        private bool saveButtonClicked = false;
        private bool removeButtonClicked = false;
        private bool quitButtonClicked = false;

        private authorService auService;
        private bookService bkService;
        private publisherService pubService;
        public Form1()
        {
            InitializeComponent();

            loadAuthorListView();
            loadBookListView();
            
        }

        public void loadAuthorListView()
        {
            //Set up the visuals for the list view.
            lvAuthors.View = View.Details;
            lvAuthors.GridLines = true;
            lvAuthors.FullRowSelect = true;
            lvAuthors.Clear();

            ListViewItem lvi;
            auService = new authorService(new AuthorRepositoryDB());
            lvAuthors.Clear();
            List<authorViewModel> authors = auService.getAllAuthors();

            foreach (authorViewModel auth in authors)
            {
                lvi = new ListViewItem(auth.ID);

                lvi.SubItems.Add(auth.LastName);
                lvi.SubItems.Add(auth.FirstName);
                lvi.SubItems.Add(auth.Phone);
                lvi.SubItems.Add(auth.Address);
                lvi.SubItems.Add(auth.City);
                lvi.SubItems.Add(auth.State);
                lvi.SubItems.Add(auth.Zip);
                lvi.SubItems.Add(auth.Contract);

                lvAuthors.Items.Add(lvi);
            }

            //Create columns
            lvAuthors.Columns.Add("ID");
            lvAuthors.Columns.Add("Last Name");
            lvAuthors.Columns.Add("First Name");
            lvAuthors.Columns.Add("Phone");
            lvAuthors.Columns.Add("Address");
            lvAuthors.Columns.Add("City");
            lvAuthors.Columns.Add("State");
            lvAuthors.Columns.Add("Zip Code");
            lvAuthors.Columns.Add("Contract");

            //Size columns
            lvAuthors.Columns[0].Width = -2;
            lvAuthors.Columns[1].Width = -2;
            lvAuthors.Columns[2].Width = -2;
            lvAuthors.Columns[3].Width = -2;
            lvAuthors.Columns[4].Width = -2;
            lvAuthors.Columns[5].Width = -2;
            lvAuthors.Columns[6].Width = -2;
            lvAuthors.Columns[7].Width = -2;
            lvAuthors.Columns[8].Width = -2;

            lvAuthors.Refresh();

        }

        public void loadBookListView()
        {
            //Set up the visuals for the list view.
            lvAllBooks.View = View.Details;
            lvAllBooks.GridLines = true;
            lvAllBooks.FullRowSelect = true;
            lvAllBooks.Clear();

            ListViewItem lvi;
            bkService = new bookService(new BookRepositoryDB());
            pubService = new publisherService(new PublisherRepositoryDB());
            lvAllBooks.Clear();
            List<bookViewModel> books = bkService.getAllBooks();
            List<pubViewModel> pubs = pubService.getAllPublishers();

            //books = books.Where(c => pubs.Any(x => x.PubID == c.PubID)).ToList();
            
            foreach (bookViewModel bk in books)
            {
                lvi = new ListViewItem(bk.TitleID);
                var name = pubs.Where(c => c.PubID == bk.PubID).FirstOrDefault();

                lvi.SubItems.Add(bk.TitleID);
                lvi.SubItems.Add(bk.Title);
                lvi.SubItems.Add(bk.Type);
                lvi.SubItems.Add(!string.IsNullOrEmpty(name.PubName) ? name.PubName : "");
                lvi.SubItems.Add(bk.Price);
                lvi.SubItems.Add(bk.PubDate);
                

                lvAllBooks.Items.Add(lvi);
            }

            //Create columns
            lvAllBooks.Columns.Add("Title ID");
            lvAllBooks.Columns.Add("Title");
            lvAllBooks.Columns.Add("Type");
            lvAllBooks.Columns.Add("Pub Name");
            lvAllBooks.Columns.Add("Price");
            lvAllBooks.Columns.Add("Pub Date");

            //Size columns
            lvAllBooks.Columns[0].Width = -2;
            lvAllBooks.Columns[1].Width = -2;
            lvAllBooks.Columns[2].Width = -2;
            lvAllBooks.Columns[3].Width = -2;
            lvAllBooks.Columns[4].Width = -2;
            lvAllBooks.Columns[5].Width = -2;

            lvAllBooks.Refresh();

        }

        public void loadBooksByAuthorView()
        {
            //Set up the visuals for the list view.
            lvAuthorBooks.View = View.Details;
            lvAuthorBooks.GridLines = true;
            lvAuthorBooks.FullRowSelect = true;
            lvAuthorBooks.Clear();

            ListViewItem auID = new ListViewItem();
            auID = lvAuthors.SelectedItems[0];

            ListViewItem lvi;
            pubService = new publisherService(new PublisherRepositoryDB());
            lvAuthorBooks.Clear();
            List<pubViewModel> pubs = pubService.getAllPublishers();
            bkService = new bookService(new BookRepositoryDB());
            List<bookViewModel> books = bkService.findByAuthor(auID.SubItems[0].Text);

            //books = books.Where(c => pubs.Any(x => x.PubID == c.PubID)).ToList();

            foreach (bookViewModel bk in books)
            {
                lvi = new ListViewItem(bk.TitleID);
                var name = pubs.Where(c => c.PubID == bk.PubID).FirstOrDefault();

                lvi.SubItems.Add(bk.Title);
                lvi.SubItems.Add(bk.Type);
                lvi.SubItems.Add(!string.IsNullOrEmpty(name.PubName) ? name.PubName : "");
                lvi.SubItems.Add(bk.Price);
                lvi.SubItems.Add(bk.PubDate);


                lvAuthorBooks.Items.Add(lvi);
            }

            //Create columns
            lvAuthorBooks.Columns.Add("Title ID");
            lvAuthorBooks.Columns.Add("Title");
            lvAuthorBooks.Columns.Add("Type");
            lvAuthorBooks.Columns.Add("Pub Name");
            lvAuthorBooks.Columns.Add("Price");
            lvAuthorBooks.Columns.Add("Pub Date");

            //Size columns
            lvAuthorBooks.Columns[0].Width = -2;
            lvAuthorBooks.Columns[1].Width = -2;
            lvAuthorBooks.Columns[2].Width = -2;
            lvAuthorBooks.Columns[3].Width = -2;
            lvAuthorBooks.Columns[4].Width = -2;
            lvAuthorBooks.Columns[5].Width = -2;

            lvAuthorBooks.Refresh();

        }

        public string getSetting(string key)
        {
            // adapted from: 
            // https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
            // see app.config file for how to add the setting

            string setting = "";

            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                setting = appSettings[key] ?? "Not Found"; // if appSettings[key] != null, then appSettings[key], else "Not found"
            }
            catch (ConfigurationErrorsException)
            {
                setting = "ERROR: Not found";
            }

            return setting;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!quitButtonClicked)
            {
                if (newButtonClicked == true && !saveButtonClicked)
                {
                    DialogResult dialogResult = MessageBox.Show("Exit without saving changes?", "Really Close?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        e.Cancel = false;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            quitButtonClicked = true;
            if (newButtonClicked == true && !saveButtonClicked)
            {
                DialogResult dialogResult = MessageBox.Show("Exit without saving changes?", "Really Close?", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    this.Close();
                }
                else if (dialogResult == DialogResult.No)
                {
                    quitButtonClicked = false;
                    return;
                }
            }
            else
            {
                this.Close();
            }
        }

        private void addBookButton_Click(object sender, EventArgs e)
        {
            saveButton.Enabled = true;
            saveButtonClicked = false;
            addBookButton.Enabled = false;

            titleIDBox.Enabled = true;
            titleBox.Enabled = true;
            typeBox.Enabled = true;
            pubIdBox.Enabled = true;
            priceBox.Enabled = true;
            pubIdBox.Enabled = true;
            dateTimePicker1.Enabled = true;

            titleIDBox.ReadOnly = false;
            titleBox.ReadOnly = false;
            typeBox.ReadOnly = false;
            pubIdBox.ReadOnly = false;
            priceBox.ReadOnly = false;
            pubIdBox.ReadOnly = false;

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            addBookButton.Enabled = true;
            saveButton.Enabled = false;
            ListViewItem bookInfo = new ListViewItem();
            bool errorOccured = false;

            titleIDBox.Enabled = false;
            titleBox.Enabled = false;
            typeBox.Enabled = false;
            pubIdBox.Enabled = false;
            priceBox.Enabled = false;
            pubIdBox.Enabled = false;
            dateTimePicker1.Enabled = false;

            titleIDBox.ReadOnly = true;
            titleBox.ReadOnly = true;
            typeBox.ReadOnly = true;
            pubIdBox.ReadOnly = true;
            priceBox.ReadOnly = true;
            pubIdBox.ReadOnly = true;

            //Check the format of the text boxes
            Match matchBookID = Regex.Match(titleIDBox.Text, "^[A-Za-z][A-Za-z][0-9][0-9][0-9][0-9]$");
            Match matchPubID = Regex.Match(pubIdBox.Text, "^[0-9a-zA-Z][0-9a-zA-Z][0-9a-zA-Z][0-9a-zA-Z]$");

            if(!matchBookID.Success)
            {
                MessageBox.Show("Please enter the Title ID number as: \n" +
                    "Az####");
                errorOccured = true;
            }

            else if (!matchPubID.Success)
            {
                MessageBox.Show("Please enter the Pub ID as: \n" +
                    "####");
                errorOccured = true;
            }

            try
            {
                double.Parse(priceBox.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter a valid price for the book");
                errorOccured = true;
            }

            //Check for empty text fields

            if(string.IsNullOrWhiteSpace(titleIDBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid Title ID");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(titleBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid Title");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(typeBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid Type");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(pubIdBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid Pub ID");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(priceBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid Price");
                errorOccured = true;
            }

            //If no errors occured, add the new book

            if (!errorOccured)
            {
                bookViewModel bkVM = new bookViewModel(titleIDBox.Text.Trim(), titleBox.Text.Trim(), typeBox.Text.Trim(),
                                                       pubIdBox.Text.Trim(), priceBox.Text.Trim(), dateTimePicker1.Text.Trim());
                bkService.addBook(bkVM);
            }

            saveButtonClicked = true;
            loadBookListView();
        }

        private void priceBox_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void lvAuthors_MouseClick(object sender, MouseEventArgs e)
        {
            loadBooksByAuthorView();
        }

        private void bookToAuButton_Click(object sender, EventArgs e)
        {
            ListViewItem bookSelected = new ListViewItem();
            bookSelected = lvAllBooks.SelectedItems[0];
            ListViewItem authorSelected = new ListViewItem();
            authorSelected = lvAuthors.SelectedItems[0];

            bkService.addToAuthor(authorSelected.SubItems[0].Text, bookSelected.SubItems[0].Text);

            loadBooksByAuthorView();
        }

        private void removeBookButton_Click(object sender, EventArgs e)
        {
            ListViewItem bookSelected = new ListViewItem();
            bookSelected = lvAuthorBooks.SelectedItems[0];
            ListViewItem authorSelected = new ListViewItem();
            authorSelected = lvAuthors.SelectedItems[0];

            bkService.removeFromAuthor(authorSelected.SubItems[0].Text, bookSelected.SubItems[0].Text);

            loadBooksByAuthorView();
        }
    }
}
