﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;
using Service;
using Repository;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace wpfLab
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<authorViewModel> AuthorCollection;
        List<authorViewModel> AuthorList;
        AuthorRepositoryDB auRepository = new AuthorRepositoryDB();
        authorService auService;
        authorViewModel selectedAuthor;

        public MainWindow()
        {
            auService = new authorService(auRepository);

            if(auService != null)
            {
                AuthorList = auService.getAllAuthors();

                if(AuthorList != null)
                {
                    AuthorCollection = new ObservableCollection<authorViewModel>(AuthorList);
                    
                    InitializeComponent();

                    dGridAuthors.ItemsSource = AuthorCollection;
                    dGridAuthors.IsReadOnly = true;

                    idTextBox.Text = idNumber();
                }
            }
        }

        public string idNumber()
        {
            Random slumpGenerator = new Random();
            int idValue1 = slumpGenerator.Next(100, 999);
            int idValue2 = slumpGenerator.Next(10, 99);
            int idValue3 = slumpGenerator.Next(1000, 9999);
            string id = idValue1.ToString() + "-" + idValue2.ToString() + "-" + idValue3.ToString();

            return id;
        }

        private void refreshList()
        {
            auService = new authorService(auRepository);

            if (auService != null)
            {
                AuthorList = auService.getAllAuthors();

                if (AuthorList != null)
                {
                    AuthorCollection = new ObservableCollection<authorViewModel>(AuthorList);

                    InitializeComponent();

                    dGridAuthors.ItemsSource = AuthorCollection;
                    dGridAuthors.IsReadOnly = true;

                    idTextBox.Text = idNumber();
                    firstNameTextBox.Text = "";
                    lastNameTextBox.Text = "";
                    phoneTextBox.Text = "";
                    addressTextBox.Text = "";
                    cityTextBox.Text = "";
                    stateTextBox.Text = "";
                    zipTextBox.Text = "";
                    contractComboBox.SelectedValue = "Yes";
                }
            }
        }

        private bool errorCheck()
        {
            bool errorOccured = false;

            //Check the format of the text boxes
            Match matchID = Regex.Match(idTextBox.Text, "^[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]$");
            Match matchPhone = Regex.Match(phoneTextBox.Text, "^[0-9][0-9][0-9] [0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$");
            Match matchState = Regex.Match(stateTextBox.Text, "^[A-Z][A-Z]$");
            Match matchZip = Regex.Match(zipTextBox.Text, "^[0-9][0-9][0-9][0-9][0-9]$");

            if (!matchID.Success)
            {
                MessageBox.Show("Please enter the ID number as: \n" +
                    "###-##-####");
                errorOccured = true;
            }

            else if (!matchPhone.Success)
            {
                MessageBox.Show("Please enter the phone number as: \n" +
                    "### ###-####");
                errorOccured = true;
            }

            else if (!matchState.Success)
            {
                MessageBox.Show("Please enter the state as two letters.");
                errorOccured = true;
            }

            else if (!matchZip.Success)
            {
                MessageBox.Show("Please enter the zip code: \n" +
                    "#####");
                errorOccured = true;
            }

            //Check for empty fields
            if (string.IsNullOrWhiteSpace(idTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid ID");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(lastNameTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid last name");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(firstNameTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid first name");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(addressTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid address");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(cityTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid city");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(stateTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid state");
                errorOccured = true;
            }

            else if ((string)contractComboBox.SelectedValue != "Yes" && (string)contractComboBox.SelectedValue != "No")
            {
                MessageBox.Show("Please select either yes or no for contract");
                errorOccured = true;
            }

            return errorOccured;
        }

        private void dGridAuthors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(dGridAuthors.SelectedItem == null)
            {
                return;
            }

            DataGrid dg = sender as DataGrid;
            selectedAuthor = (authorViewModel)dg.SelectedItem;
            addButton.IsEnabled = false;

            idTextBox.Text = selectedAuthor.ID;
            firstNameTextBox.Text = selectedAuthor.FirstName;
            lastNameTextBox.Text = selectedAuthor.LastName;
            phoneTextBox.Text = selectedAuthor.Phone;
            addressTextBox.Text = selectedAuthor.Address;
            cityTextBox.Text = selectedAuthor.City;
            stateTextBox.Text = selectedAuthor.State;
            zipTextBox.Text = selectedAuthor.Zip;

            if (selectedAuthor.Contract == "Yes")
                contractComboBox.SelectedValue = "Yes";

            else
                contractComboBox.SelectedValue = "No";
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            bool errorOccured = false;

            errorOccured = errorCheck();

            //If no errors save the new info
            if (!errorOccured)
            {

                if ((string)contractComboBox.SelectedValue == "Yes")
                {
                    authorViewModel authorToAdd = new authorViewModel(idTextBox.Text, lastNameTextBox.Text, firstNameTextBox.Text, phoneTextBox.Text,
                                                                  addressTextBox.Text, cityTextBox.Text, stateTextBox.Text, zipTextBox.Text, "Yes");
                    auService.addAuthor(authorToAdd);


                    refreshList();
                }
                else
                {
                    authorViewModel authorToAdd = new authorViewModel(idTextBox.Text, lastNameTextBox.Text, firstNameTextBox.Text, phoneTextBox.Text,
                                                                  addressTextBox.Text, cityTextBox.Text, stateTextBox.Text, zipTextBox.Text, "No");
                    auService.addAuthor(authorToAdd);

                    refreshList();
                }
            }
            
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            bool errorOccured = false;

            errorOccured = errorCheck();

            //If no errors save the new info
            if (!errorOccured)
            {
                if ((string)contractComboBox.SelectedValue == "Yes")
                {
                    authorViewModel authorToUpdate = new authorViewModel(idTextBox.Text, lastNameTextBox.Text, firstNameTextBox.Text, phoneTextBox.Text,
                                                                  addressTextBox.Text, cityTextBox.Text, stateTextBox.Text, zipTextBox.Text, "Yes");
                    auService.updateAuthor(authorToUpdate);

                    refreshList();
                }
                else
                {
                    authorViewModel authorToUpdate = new authorViewModel(idTextBox.Text, lastNameTextBox.Text, firstNameTextBox.Text, phoneTextBox.Text,
                                                                  addressTextBox.Text, cityTextBox.Text, stateTextBox.Text, zipTextBox.Text, "No");
                    auService.updateAuthor(authorToUpdate);

                    refreshList();
                }
            }
        }

        private void removeButton_Click(object sender, RoutedEventArgs e)
        {
            if ((string)contractComboBox.SelectedValue == "Yes")
            {
                authorViewModel authorToRemove = new authorViewModel(idTextBox.Text, lastNameTextBox.Text, firstNameTextBox.Text, phoneTextBox.Text,
                                                              addressTextBox.Text, cityTextBox.Text, stateTextBox.Text, zipTextBox.Text, "Yes");
                auService.removeAuthor(authorToRemove);

                refreshList();
            }
            else
            {
                authorViewModel authorToRemove = new authorViewModel(idTextBox.Text, firstNameTextBox.Text, lastNameTextBox.Text, phoneTextBox.Text,
                                                              addressTextBox.Text, cityTextBox.Text, stateTextBox.Text, zipTextBox.Text, "No");
                auService.removeAuthor(authorToRemove);

                refreshList();
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            addButton.IsEnabled = true;

            idTextBox.Text = idNumber();
            firstNameTextBox.Text = "";
            lastNameTextBox.Text = "";
            phoneTextBox.Text = "";
            addressTextBox.Text = "";
            cityTextBox.Text = "";
            stateTextBox.Text = "";
            zipTextBox.Text = "";
            contractComboBox.SelectedValue = "Yes";
        }

        private void quitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
