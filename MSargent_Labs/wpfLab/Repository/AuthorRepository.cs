﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Configuration;
using Model;

using System.Data.SqlClient;

namespace Repository
{
    public class returnResult
    {
        public bool success { get; set; }
        public bool message { get; set; }
    }

    public class authorGetResult : returnResult
    {
        public List<author> authors { get; set; }
    }

    public class authorSetResult : returnResult
    {
        // nothing to add 
    }

    public class bookGetResult : returnResult
    {
        public List<book> books { get; set; }
    }

    public class publisherGetResult : returnResult
    {
        public List<publisher> pubs { get; set; }
    }

    public interface IRepository<T>
    {
        List<T> FindAll();
        T FindByID(string id);
        bool Add(T x);
        bool Update(T x);
        bool Remove(T x);
    }

    class configFile
    {
        public static string getSetting(string key)
        {
            // adapted from: 
            // https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
            // see app.config file for how to add the setting

            string setting = "";

            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                setting = appSettings[key] ?? "Not Found"; // if appSettings[key] != null, then appSettings[key], else "Not found"
            }
            catch (ConfigurationErrorsException)
            {
                //setting = "ERROR: Not found";
            }

            return setting;
        }
    }


    public class AuthorRepositoryDB : IRepository<author>
    {
        private string _connectionString;

        public AuthorRepositoryDB()
        {
            _connectionString = configFile.getSetting("pubsDBConnectionString");
            if (_connectionString == "ERROR: Not found")
            {
                Environment.Exit(-1);
            }
        }

        public author FindByID(string id)
        {
            author au = null;

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM authors WHERE au_id = @id", connection))
                    {
                        command.Parameters.Add(new SqlParameter("@id", id));

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {

                                au = new author(dataReader["au_id"].ToString(),
                                                dataReader["au_fname"].ToString(),
                                                dataReader["au_lname"].ToString(),
                                                dataReader["phone"].ToString(),
                                                dataReader["address"].ToString(),
                                                dataReader["city"].ToString(),
                                                dataReader["state"].ToString(),
                                                dataReader["zip"].ToString(),
                                                (bool)dataReader["contract"]); // dataReader["contract"]); 
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(author);
            }

            return au;
        }

        public bool Add(author au)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "INSERT INTO authors VALUES(@id, @fname, @lname, @phone, @addr, @city, @state, @zip, @contract)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@id", au.id));
                        command.Parameters.Add(new SqlParameter("@fname", au.fname));
                        command.Parameters.Add(new SqlParameter("@lname", au.lname));
                        command.Parameters.Add(new SqlParameter("@phone", au.phone));
                        command.Parameters.Add(new SqlParameter("@addr", au.address));
                        command.Parameters.Add(new SqlParameter("@city", au.city));
                        command.Parameters.Add(new SqlParameter("@state", au.state));
                        command.Parameters.Add(new SqlParameter("@zip", au.zip));
                        command.Parameters.Add(new SqlParameter("@contract", au.contract));

                        if (command.ExecuteNonQuery() <= 0) return false;

                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool Update(author au)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "UPDATE authors SET  au_lname = @lname, au_fname = @fname, phone = @phone, " +
                                 "address = @addr, city = @city, state = @state, zip = @zip, contract = @contract " +
                                 "WHERE au_id = @id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@lname", au.lname);
                        command.Parameters.AddWithValue("@fname", au.fname);
                        command.Parameters.AddWithValue("@phone", au.phone);
                        command.Parameters.AddWithValue("@addr", au.address);
                        command.Parameters.AddWithValue("@city", au.city);
                        command.Parameters.AddWithValue("@state", au.state);
                        command.Parameters.AddWithValue("@zip", au.zip);
                        command.Parameters.AddWithValue("@contract", au.contract);
                        command.Parameters.AddWithValue("@id", au.id);

                        if (command.ExecuteNonQuery() <= 0) return false;
                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool Remove(author au)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM authors WHERE au_id = @id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", au.id);
                        command.ExecuteNonQuery();
                    }// end use command;
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public List<author> FindAll()
        {
            List<author> authors = new List<author>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM authors", connection))
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                author au = new author(dataReader["au_id"].ToString(),
                                                        dataReader["au_fname"].ToString(),
                                                        dataReader["au_lname"].ToString(),
                                                        dataReader["phone"].ToString(),
                                                        dataReader["address"].ToString(),
                                                        dataReader["city"].ToString(),
                                                        dataReader["state"].ToString(),
                                                        dataReader["zip"].ToString(),
                                                        (bool)dataReader["contract"]); // dataReader["contract"]); 

                                authors.Add(au);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<author>);
            }

            return authors;
        }
    }

    public class BookRepositoryDB : IRepository<book>
    {
        private string _connectionString;

        public BookRepositoryDB()
        {
            _connectionString = configFile.getSetting("pubsDBConnectionString");
            if (_connectionString == "ERROR: Not found")
            {
                Environment.Exit(-1);
            }
        }

        public book FindByID(string titleID)
        {
            book bk = null;

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM authors WHERE title_id = @titleID", connection))
                    {
                        command.Parameters.Add(new SqlParameter("@titleID", titleID));

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {

                                Decimal? price = null;
                                if (!dataReader.IsDBNull(dataReader.GetOrdinal("price")))
                                {
                                    // *** Only read field if its value is not null
                                    price = (Decimal)dataReader["price"];
                                }
                                // *** price is either null or has a non-null value when you reach here


                                bk = new book(dataReader["title_id"].ToString(),
                                                dataReader["title"].ToString(),
                                                dataReader["type"].ToString(),
                                                dataReader["pub_id"].ToString(),
                                                price,
                                                dataReader["pubdate"].ToString()); // dataReader["contract"]); 
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(book);
            }

            return bk;
        }

        public List<book> FindByAuthor(string auID)
        {

            List<book> books = new List<book>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM titleauthor ta " +
                        "JOIN authors a ON ta.au_id = a.au_id " +
                        "JOIN titles t ON ta.title_id = t.title_id " +
                        "WHERE ta.au_id = @auID", connection))

                    {
                        command.Parameters.Add(new SqlParameter("@auID", auID));
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                Decimal? price = null;
                                if (!dataReader.IsDBNull(dataReader.GetOrdinal("price")))
                                {
                                    // *** Only read field if its value is not null
                                    price = (Decimal)dataReader["price"];
                                }
                                // *** price is either null or has a non-null value when you reach here


                                book bk = new book(dataReader["title_id"].ToString(),
                                                dataReader["title"].ToString(),
                                                dataReader["type"].ToString(),
                                                dataReader["pub_id"].ToString(),
                                                price,
                                                dataReader["pubdate"].ToString()); // dataReader["contract"]); 

                                books.Add(bk);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<book>);
            }

            return books;

        }

        public bool AddToAuthor(string auID, string bookID)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();


                    string sql = "INSERT INTO titleauthor (au_id, title_id) VALUES(@auID, @bookID)";
                    //string sql = "INSERT INTO titles VALUES(@titleID, @title, @type, @pubID, @price, @pubDate)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@auID", auID));
                        command.Parameters.Add(new SqlParameter("@bookID", bookID));

                        if (command.ExecuteNonQuery() <= 0) return false;

                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool RemoveFromAuthor(string auID, string bookID)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM titleauthor WHERE au_id = @auID AND title_id = @bookID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@auID", auID);
                        command.Parameters.AddWithValue("@bookID", bookID);
                        command.ExecuteNonQuery();
                    }// end use command;
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool Add(book bk)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();


                    string sql = "INSERT INTO titles (title_id, title, type, pub_id, price, pubdate) " +
                        "VALUES(@titleID, @title, @type, @pubID, @price, @pubdate)";
                    //string sql = "INSERT INTO titles VALUES(@titleID, @title, @type, @pubID, @price, @pubDate)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@titleID", bk.TitleID));
                        command.Parameters.Add(new SqlParameter("@title", bk.Title));
                        command.Parameters.Add(new SqlParameter("@type", bk.Type));
                        command.Parameters.Add(new SqlParameter("@pubID", bk.PubID));
                        command.Parameters.Add(new SqlParameter("@price", bk.Price));
                        command.Parameters.Add(new SqlParameter("@pubDate", bk.PubDate));

                        if (command.ExecuteNonQuery() <= 0) return false;

                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool Update(book bk)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "UPDATE titles SET  title_id = @titleID, title = @title, type = @type, " +
                                 "pub_id = @pubID, price = @price, pubdate = @pubDate " +
                                 "WHERE title_id = @titleID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@titleID", bk.TitleID);
                        command.Parameters.AddWithValue("@title", bk.Title);
                        command.Parameters.AddWithValue("@type", bk.Type);
                        command.Parameters.AddWithValue("@pubID", bk.PubID);
                        command.Parameters.AddWithValue("@price", bk.Price);
                        command.Parameters.AddWithValue("@pubDate", bk.PubDate);

                        if (command.ExecuteNonQuery() <= 0) return false;
                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool Remove(book bk)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM titles WHERE title_id = @titleID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@titleID", bk.TitleID);
                        command.ExecuteNonQuery();
                    }// end use command;
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public List<book> FindAll()
        {
            List<book> books = new List<book>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM titles", connection))
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                Decimal? price = null;
                                if (!dataReader.IsDBNull(dataReader.GetOrdinal("price")))
                                {
                                    // *** Only read field if its value is not null
                                    price = (Decimal)dataReader["price"];
                                }
                                // *** price is either null or has a non-null value when you reach here


                                book bk = new book(dataReader["title_id"].ToString(),
                                                dataReader["title"].ToString(),
                                                dataReader["type"].ToString(),
                                                dataReader["pub_id"].ToString(),
                                                price,
                                                dataReader["pubdate"].ToString()); // dataReader["contract"]); 

                                books.Add(bk);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<book>);
            }

            return books;
        }
    }

    public class PublisherRepositoryDB : IRepository<publisher>
    {
        private string _connectionString;

        public PublisherRepositoryDB()
        {
            _connectionString = configFile.getSetting("pubsDBConnectionString");
            if (_connectionString == "ERROR: Not found")
            {
                Environment.Exit(-1);
            }
        }

        public publisher FindByID(string pubID)
        {
            publisher pub = null;

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM publishers WHERE pub_id = @pubID", connection))
                    {
                        command.Parameters.Add(new SqlParameter("@pubID", pubID));

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {

                                pub = new publisher(dataReader["pub_id"].ToString(),
                                                dataReader["pub_name"].ToString(),
                                                dataReader["city"].ToString(),
                                                dataReader["state"].ToString(),
                                                dataReader["country"].ToString()); // dataReader["contract"]); 
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(publisher);
            }

            return pub;
        }

        public bool Add(publisher pub)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "INSERT INTO publisher VALUES(@pubID, @pubName, @city, @state, @country)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@pubID", pub.PubID));
                        command.Parameters.Add(new SqlParameter("@pubName", pub.PubName));
                        command.Parameters.Add(new SqlParameter("@city", pub.City));
                        command.Parameters.Add(new SqlParameter("@state", pub.State));
                        command.Parameters.Add(new SqlParameter("@country", pub.Country));

                        if (command.ExecuteNonQuery() <= 0) return false;

                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool Update(publisher pub)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "UPDATE publisers SET  pub_id = @pubID, pub_name = @pubName, city = @city, " +
                                 "state = @state, country = @country " +
                                 "WHERE pub_id = @pubID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@pubID", pub.PubID);
                        command.Parameters.AddWithValue("@pubName", pub.PubName);
                        command.Parameters.AddWithValue("@city", pub.City);
                        command.Parameters.AddWithValue("@state", pub.State);
                        command.Parameters.AddWithValue("@country", pub.Country);

                        if (command.ExecuteNonQuery() <= 0) return false;
                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool Remove(publisher pub)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM publishers WHERE pub_id = @pubID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@pubID", pub.PubID);
                        command.ExecuteNonQuery();
                    }// end use command;
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public List<publisher> FindAll()
        {
            List<publisher> pubs = new List<publisher>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM publishers", connection))
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                publisher pub = new publisher(dataReader["pub_id"].ToString(),
                                                        dataReader["pub_name"].ToString(),
                                                        dataReader["city"].ToString(),
                                                        dataReader["state"].ToString(),
                                                        dataReader["country"].ToString()); // dataReader["contract"]); 

                                pubs.Add(pub);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<publisher>);
            }

            return pubs;
        }
    }

}
