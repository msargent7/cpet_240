﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Model;
using Repository; 

namespace Service
{
    public class authorService
    {
        IRepository<author> _repository;

        public authorService(IRepository<author> repository)
        {
            _repository = repository; 
        }

        public List<authorViewModel> getAllAuthors()
        {
            List<authorViewModel> authors = new List<authorViewModel>();

            List<author> auList = _repository.FindAll(); 

            foreach(author au in auList)
            {
                authors.Add(viewModelFromAuthor(au)); 
            }

            return authors; 
        }

        public bool updateAuthor(author au)
        {
            // or pass in author view model and convert to author
            // should the UI layer be exposed to authors or author view model only ?????
            if(_repository.Update(au) == true)
            {
                return true; 
            }

            return false;
        }

        public bool addAuthor(author au)
        {
            if(_repository.FindByID(au.id) == null)
            {
                return _repository.Add(au); 
            }

            return false; 
        }

        public author getAuthorByID(string au_id)
        {
            return _repository.FindByID(au_id); 
        }

        public authorViewModel viewModelFromAuthor(author au)
        {
            authorViewModel a = new authorViewModel(au.id);

            a.FirstName = au.fname;
            a.LastName = au.lname;
            a.Phone = au.phone;
            a.Zip = au.zip;
            a.State = au.state;
            a.City = au.city;
            a.Address = au.address; 

            if(au.contract) a.Contract = "Yes";
            else a.Contract = "No"; 
            
            return a; 
        }
    }

    public class authorViewModel
    {
        // the view model class is for creating a display friendly object
        // usually, class properites are converted to strings 

        /*
        private string _id;
        private string _fname;
        private string _lname;
        private string _phone;
        private string _address;
        private string _city;
        private string _state;
        private string _zip;
        private Boolean _contract; 
         */

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ID { get; private set; }
        public string Name { get { return FirstName + " " + LastName; } }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Contract { get; set; }

        public authorViewModel(string id)
        {
            ID = id; 
        }
    }
}
