﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Configuration; 
using Model;

using System.Data.SqlClient;
using System.Xml;

namespace Repository
{
    public class returnResult
    {
        public bool success { get; set; }
        public bool message { get; set; }
    }

    public class authorGetResult : returnResult
    {
        public List<author> authors { get; set; }
    }

    public class authorSetResult : returnResult
    {
        // nothing to add 
    }

    public interface IRepository<T>
    {
        List<T> FindAll();
        T FindByID(string id); 
        bool Add(T x); 
        bool Update(T x); 
        bool Remove(T x); 
    }

    class configFile
    {
        public static string getSetting(string key)
        {
            // adapted from: 
            // https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
            // see app.config file for how to add the setting

            string setting = "";

            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                setting = appSettings[key] ?? "Not Found"; // if appSettings[key] != null, then appSettings[key], else "Not found"
            }
            catch (ConfigurationErrorsException)
            {
                setting = "ERROR: Not found";
            }

            return setting;
        }
    }

    public class AuthorRepository : IRepository<author>
    {
        public author FindByID(string id)
        {
            List<author> authors = FindAll();

            foreach (author au in authors) {
                if (au.id == id) return au;
            }

            return default(author); 

        }

        public bool Add(author au)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("authors.xml");

            XmlNode node = doc.CreateElement("author");
            XmlNode contact = doc.CreateElement("contact");
            XmlAttribute au_id = doc.CreateAttribute("au_id");

            au_id.InnerText = au.id;
            node.Attributes.Append(au_id);

            node.AppendChild(doc.CreateElement("au_lname"));
            node["au_lname"].InnerText = au.lname;

            node.AppendChild(doc.CreateElement("au_fname"));
            node["au_fname"].InnerText = au.fname;

            contact.AppendChild(doc.CreateElement("phone"));
            contact["phone"].InnerText = au.phone;

            contact.AppendChild(doc.CreateElement("address"));
            contact["address"].InnerText = au.address;

            contact.AppendChild(doc.CreateElement("city"));
            contact["city"].InnerText = au.city;

            contact.AppendChild(doc.CreateElement("state"));
            contact["state"].InnerText = au.state;

            contact.AppendChild(doc.CreateElement("zip"));
            contact["zip"].InnerText = au.zip;


            node.AppendChild(contact);

            XmlNode root = doc.SelectSingleNode("authors");

            root.AppendChild(node);

            doc.Save("authors.xml");


            return true;
        }

        public bool Update(author au)
        {
            if (Remove(au)) {
                if(Add(au)) return true;
                return true; 
            }

            return false; 
        }

        public bool Remove(author au)
        {
            XmlDocument xDoc = new XmlDocument();

            xDoc.Load("authors.xml");

            XmlNode node = xDoc.SelectSingleNode("authors/author[@au_id='"+au.id +"']");

            node.ParentNode.RemoveChild(node);    

            return true; 
        }

        public List<author> FindAll()
        {
            List<author> authors = new List<author>();

            // @ means ignore escape sequences
            using (XmlReader reader = XmlReader.Create("authors.xml"))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        if (reader.Name == "author")
                        {
                            
                            author au = new author();

                            au.id = reader.GetAttribute("au_id");

                            reader.Read();

                            while (reader.Name != "author") {
                                if (reader.NodeType == XmlNodeType.Element)
                                {
                                    switch (reader.Name)
                                    {
                                        case "au_lname":
                                            au.lname = reader.ReadInnerXml();
                                            break;
                                        case "au_fname":
                                            au.fname = reader.ReadInnerXml();
                                            break;
                                        case "phone":
                                            au.phone = reader.ReadInnerXml();
                                            break;
                                        case "address":
                                            au.address = reader.ReadInnerXml();
                                            break;
                                        case "city":
                                            au.city = reader.ReadInnerXml();
                                            break;
                                        case "state":
                                            au.state = reader.ReadInnerXml();
                                            break;
                                        case "zip":
                                            au.zip = reader.ReadInnerXml();
                                            break;
                                        case "contract":
                                            au.contract = false;
                                            if (reader.ReadInnerXml() == "1") au.contract = true;
                                            break;
                                        default:
                                            reader.Read();
                                            break;
                                    }
                                }
                                else {
                                    reader.Read();
                                }
                            }

                            authors.Add(au);
                        }
                    }// found start element
                }// end read loop

            }// end of using reader

            return authors; 
        }
    }


}
