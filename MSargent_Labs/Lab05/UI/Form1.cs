﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using Repository;
using Service;
using Model;

namespace UI
{
    public partial class Form1 : Form
    {
        private static AuthorRepository auRepo = new AuthorRepository();
        private static authorService auServ = new authorService(auRepo);

        public Form1()
        {
            InitializeComponent();
            initializeLV();
            listOnFile();

            
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;

        }

        private void initializeLV()
        {

            button3.Enabled = false;

            Authors_ListView.View = View.Details;
            Authors_ListView.GridLines = true;
            Authors_ListView.FullRowSelect = true;
            Authors_ListView.FullRowSelect = true;
            Authors_ListView.Sorting = SortOrder.Ascending;

            Authors_ListView.Columns.Add("ID");
            Authors_ListView.Columns.Add("Last Name");
            Authors_ListView.Columns.Add("First Name");
            Authors_ListView.Columns.Add("Phone");
            Authors_ListView.Columns.Add("Address");
            Authors_ListView.Columns.Add("City");
            Authors_ListView.Columns.Add("State");
            Authors_ListView.Columns.Add("Zip");
            Authors_ListView.Columns.Add("Contract");

            Authors_ListView.Columns[0].Width = -2;
            Authors_ListView.Columns[1].Width = -2;
        }

        private void listOnFile() {

            Authors_ListView.Items.Clear();

            ListViewItem lvi;
        
            List<authorViewModel> authors = auServ.getAllAuthors();

            foreach (authorViewModel auth in authors)
            {
                lvi = new ListViewItem(auth.ID);

                lvi.SubItems.Add(auth.LastName);
                lvi.SubItems.Add(auth.FirstName);
                lvi.SubItems.Add(auth.Phone);
                lvi.SubItems.Add(auth.Address);
                lvi.SubItems.Add(auth.City);
                lvi.SubItems.Add(auth.State);
                lvi.SubItems.Add(auth.Zip);
                lvi.SubItems.Add(auth.Contract);

                Authors_ListView.Items.Add(lvi);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random rng = new Random();
            ID_Textbox.Text = Convert.ToString(rng.Next(100, 999)) + "-" + Convert.ToString(rng.Next(10, 99)) + "-" + Convert.ToString(rng.Next(1000, 9999));

            if (!checkFields()) {
                return;
            }

            if (!auServ.addAuthor(pullData()))
            {
                MessageBox.Show("Error adding");
            }
            

            clearFields();
            listOnFile();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = true;

            ListViewItem selectedItem = Authors_ListView.SelectedItems[0];

            if (selectedItem == null) {
                MessageBox.Show("No Item Selected");
                return;
            }

            ID_Textbox.Text = selectedItem.Text;
            Lname_Textbox.Text = selectedItem.SubItems[1].Text;
            FName_Textbox.Text = selectedItem.SubItems[2].Text;
            phone_Textbox.Text = selectedItem.SubItems[3].Text;
            Address_Textbox.Text = selectedItem.SubItems[4].Text;
            City_Textbox.Text = selectedItem.SubItems[5].Text;
            State_Textbox.Text = selectedItem.SubItems[6].Text;
            Zip_Textbox.Text = selectedItem.SubItems[7].Text;

            No_RadioButton.Checked = true;
            if (selectedItem.SubItems[8].Text == "Yes") Yes_RadioButton.Checked = true;

        }

        private void button3_Click(object sender, EventArgs e)
        {
          
            button3.Enabled = false;
            button2.Enabled = true;

            if (!auServ.updateAuthor(pullData())) {
                MessageBox.Show("Error updating");
            }

            clearFields();
            listOnFile();
        }

        private author pullData()
        {
            author au = new author(ID_Textbox.Text,
                                   FName_Textbox.Text,
                                   Lname_Textbox.Text,
                                   phone_Textbox.Text,
                                   Address_Textbox.Text,
                                   City_Textbox.Text,
                                   State_Textbox.Text,
                                   Zip_Textbox.Text,
                                   Yes_RadioButton.Checked);
            return au;
        }

        private bool checkFields() {
            
            if (FName_Textbox.Text.Trim() == "") {
                MessageBox.Show("Please enter a first name");
                return false;
            }
            if (Lname_Textbox.Text.Trim() == "")
            {
                MessageBox.Show("Please enter a last name");
                return false;

            }
            if (phone_Textbox.Text.Trim() == "" || phone_Textbox.Text.Length < 10)
            {
                MessageBox.Show("Please enter a valid phone # number");
                return false;
            }
            if (Address_Textbox.Text.Trim() == "")
            {
                MessageBox.Show("Please enter an address");
                return false;
            }
            if (City_Textbox.Text.Trim() == "")
            {
                MessageBox.Show("Please enter a city name");
                return false;
            }
            if (State_Textbox.Text.Trim() == "")
            {
                MessageBox.Show("Please enter a state");
                return false;
            }
            if (Zip_Textbox.Text.Trim() == "")
            {
                MessageBox.Show("Please enter a zip code");
                return false;
            }
            
            return true;
        }

        private void clearFields()
        {
            ID_Textbox.Text = "";
            FName_Textbox.Text = "";
            Lname_Textbox.Text = "";
            phone_Textbox.Text = "";
            Address_Textbox.Text = "";
            City_Textbox.Text = "";
            State_Textbox.Text = "";
            Zip_Textbox.Text = "";
            Yes_RadioButton.Checked = true;
        }

        private void Lname_Textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char pressedKey = e.KeyChar;

            e.Handled = true;

            if (pressedKey == '-') e.Handled = false;

            if (Char.IsLetter(pressedKey) || Char.IsControl(pressedKey) || Char.IsWhiteSpace(pressedKey))
            {
                e.Handled = false;
            }
        }

        private void FName_Textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char pressedKey = e.KeyChar;

            e.Handled = true;

            if (pressedKey == '-') e.Handled = false;

            if (Char.IsLetter(pressedKey) || Char.IsControl(pressedKey) || Char.IsWhiteSpace(pressedKey))
            {
                e.Handled = false;
            }
        }

        private void phone_Textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char pressedKey = e.KeyChar;

            e.Handled = true;

            if (Char.IsNumber(pressedKey)) e.Handled = false;

            if (phone_Textbox.Text.Length >= 10) {
                e.Handled = true;
            }

            if (pressedKey == (char)8) {
                e.Handled = false;
            }
        }

        private void City_Textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char pressedKey = e.KeyChar;

            e.Handled = true;

            if (!Char.IsNumber(pressedKey)) e.Handled = false;
        }

        private void State_Textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char pressedKey = e.KeyChar;

            e.Handled = true;

            if (Char.IsLetter(pressedKey)) e.Handled = false;

            if (State_Textbox.Text.Length >= 2)
            {
                e.Handled = true;
            }

            if (pressedKey == (char)8) e.Handled = false;

        }

        private void Zip_Textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char pressedKey = e.KeyChar;

            e.Handled = true;

            if (Char.IsNumber(pressedKey)) e.Handled = false;

            if (Zip_Textbox.Text.Length >= 5)
            {
                e.Handled = true;
            }

            if (pressedKey == (char)8)
            {
                e.Handled = false;
            }

        }

        private void Authors_ListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
        }
    }
}
