﻿namespace UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Authors_ListView = new System.Windows.Forms.ListView();
            this.ID_Textbox = new System.Windows.Forms.TextBox();
            this.Lname_Textbox = new System.Windows.Forms.TextBox();
            this.FName_Textbox = new System.Windows.Forms.TextBox();
            this.phone_Textbox = new System.Windows.Forms.TextBox();
            this.Address_Textbox = new System.Windows.Forms.TextBox();
            this.City_Textbox = new System.Windows.Forms.TextBox();
            this.State_Textbox = new System.Windows.Forms.TextBox();
            this.Zip_Textbox = new System.Windows.Forms.TextBox();
            this.Yes_RadioButton = new System.Windows.Forms.RadioButton();
            this.No_RadioButton = new System.Windows.Forms.RadioButton();
            this.ID_Label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Authors_ListView
            // 
            this.Authors_ListView.Location = new System.Drawing.Point(13, 13);
            this.Authors_ListView.Name = "Authors_ListView";
            this.Authors_ListView.Size = new System.Drawing.Size(665, 444);
            this.Authors_ListView.TabIndex = 0;
            this.Authors_ListView.UseCompatibleStateImageBehavior = false;
            this.Authors_ListView.SelectedIndexChanged += new System.EventHandler(this.Authors_ListView_SelectedIndexChanged);
            // 
            // ID_Textbox
            // 
            this.ID_Textbox.Location = new System.Drawing.Point(748, 16);
            this.ID_Textbox.Name = "ID_Textbox";
            this.ID_Textbox.ReadOnly = true;
            this.ID_Textbox.Size = new System.Drawing.Size(158, 20);
            this.ID_Textbox.TabIndex = 1;
            // 
            // Lname_Textbox
            // 
            this.Lname_Textbox.Location = new System.Drawing.Point(748, 39);
            this.Lname_Textbox.Name = "Lname_Textbox";
            this.Lname_Textbox.Size = new System.Drawing.Size(158, 20);
            this.Lname_Textbox.TabIndex = 2;
            this.Lname_Textbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Lname_Textbox_KeyPress);
            // 
            // FName_Textbox
            // 
            this.FName_Textbox.Location = new System.Drawing.Point(748, 65);
            this.FName_Textbox.Name = "FName_Textbox";
            this.FName_Textbox.Size = new System.Drawing.Size(158, 20);
            this.FName_Textbox.TabIndex = 3;
            this.FName_Textbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FName_Textbox_KeyPress);
            // 
            // phone_Textbox
            // 
            this.phone_Textbox.Location = new System.Drawing.Point(748, 91);
            this.phone_Textbox.Name = "phone_Textbox";
            this.phone_Textbox.Size = new System.Drawing.Size(158, 20);
            this.phone_Textbox.TabIndex = 4;
            this.phone_Textbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.phone_Textbox_KeyPress);
            // 
            // Address_Textbox
            // 
            this.Address_Textbox.Location = new System.Drawing.Point(748, 117);
            this.Address_Textbox.Name = "Address_Textbox";
            this.Address_Textbox.Size = new System.Drawing.Size(158, 20);
            this.Address_Textbox.TabIndex = 5;
            // 
            // City_Textbox
            // 
            this.City_Textbox.Location = new System.Drawing.Point(748, 143);
            this.City_Textbox.Name = "City_Textbox";
            this.City_Textbox.Size = new System.Drawing.Size(158, 20);
            this.City_Textbox.TabIndex = 6;
            this.City_Textbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.City_Textbox_KeyPress);
            // 
            // State_Textbox
            // 
            this.State_Textbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.State_Textbox.Location = new System.Drawing.Point(748, 169);
            this.State_Textbox.Name = "State_Textbox";
            this.State_Textbox.Size = new System.Drawing.Size(158, 20);
            this.State_Textbox.TabIndex = 7;
            this.State_Textbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.State_Textbox_KeyPress);
            // 
            // Zip_Textbox
            // 
            this.Zip_Textbox.Location = new System.Drawing.Point(748, 195);
            this.Zip_Textbox.Name = "Zip_Textbox";
            this.Zip_Textbox.Size = new System.Drawing.Size(158, 20);
            this.Zip_Textbox.TabIndex = 8;
            this.Zip_Textbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Zip_Textbox_KeyPress);
            // 
            // Yes_RadioButton
            // 
            this.Yes_RadioButton.AutoSize = true;
            this.Yes_RadioButton.Checked = true;
            this.Yes_RadioButton.Location = new System.Drawing.Point(748, 221);
            this.Yes_RadioButton.Name = "Yes_RadioButton";
            this.Yes_RadioButton.Size = new System.Drawing.Size(43, 17);
            this.Yes_RadioButton.TabIndex = 9;
            this.Yes_RadioButton.TabStop = true;
            this.Yes_RadioButton.Text = "Yes";
            this.Yes_RadioButton.UseVisualStyleBackColor = true;
            // 
            // No_RadioButton
            // 
            this.No_RadioButton.AutoSize = true;
            this.No_RadioButton.Location = new System.Drawing.Point(797, 221);
            this.No_RadioButton.Name = "No_RadioButton";
            this.No_RadioButton.Size = new System.Drawing.Size(39, 17);
            this.No_RadioButton.TabIndex = 10;
            this.No_RadioButton.Text = "No";
            this.No_RadioButton.UseVisualStyleBackColor = true;
            // 
            // ID_Label
            // 
            this.ID_Label.AutoSize = true;
            this.ID_Label.Location = new System.Drawing.Point(684, 19);
            this.ID_Label.Name = "ID_Label";
            this.ID_Label.Size = new System.Drawing.Size(18, 13);
            this.ID_Label.TabIndex = 11;
            this.ID_Label.Text = "ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(684, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Last Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(684, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "First Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(684, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Phone";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(684, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Address";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(684, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "City";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(684, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "State";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(684, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Zip Code";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(684, 223);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Contract";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(687, 405);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(239, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Edit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(687, 376);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(239, 23);
            this.button2.TabIndex = 21;
            this.button2.Text = "New";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(687, 434);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(239, 23);
            this.button3.TabIndex = 22;
            this.button3.Text = "Save";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 469);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ID_Label);
            this.Controls.Add(this.No_RadioButton);
            this.Controls.Add(this.Yes_RadioButton);
            this.Controls.Add(this.Zip_Textbox);
            this.Controls.Add(this.State_Textbox);
            this.Controls.Add(this.City_Textbox);
            this.Controls.Add(this.Address_Textbox);
            this.Controls.Add(this.phone_Textbox);
            this.Controls.Add(this.FName_Textbox);
            this.Controls.Add(this.Lname_Textbox);
            this.Controls.Add(this.ID_Textbox);
            this.Controls.Add(this.Authors_ListView);
            this.Name = "Form1";
            this.Text = "Author Viewer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView Authors_ListView;
        private System.Windows.Forms.TextBox ID_Textbox;
        private System.Windows.Forms.TextBox Lname_Textbox;
        private System.Windows.Forms.TextBox FName_Textbox;
        private System.Windows.Forms.TextBox phone_Textbox;
        private System.Windows.Forms.TextBox Address_Textbox;
        private System.Windows.Forms.TextBox City_Textbox;
        private System.Windows.Forms.TextBox State_Textbox;
        private System.Windows.Forms.TextBox Zip_Textbox;
        private System.Windows.Forms.RadioButton Yes_RadioButton;
        private System.Windows.Forms.RadioButton No_RadioButton;
        private System.Windows.Forms.Label ID_Label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

