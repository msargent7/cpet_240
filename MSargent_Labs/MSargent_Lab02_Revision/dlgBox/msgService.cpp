/*
Student Name: Michael Sargent
CP240 Lab Section:  CP240-2A
Date: 9/12/17
Lab Assignment: Lab02 - msgService
Project Name: dlgBox
File Name: msgService.cpp
Description:
Limitations: 
Credits: Starter code provided by Professor Dellafelice. MSDN used as primary reference for unknown windows functions.
*/

#include <windows.h> 
#include <tchar.h>
#include <cstdio>		
#include "resource.h"  
#include <iostream>
#include <cstring>
#include <string>

using namespace std;

// function prototypes
LRESULT CALLBACK WindowFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK DialogFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
LPCSTR StatusToString(DWORD status);

// globals
// **** ProfD - resolves UNICODE LPCWCHAR and ASCII LPCHAR
const TCHAR *szWinName = TEXT("SampleWindow"); // name of window class
const unsigned int bufMaxElements = 80;
//BOOL bFileExists;
//DWORD dwBytesWritten = 0;
HANDLE hFile;
LPCSTR SZ_FILE = "C:\\zing\\time.txt";
LPCSTR SZ_PATH = "C:\\zing";
SC_HANDLE schSCManager; // handle to Service Control Manager
SC_HANDLE schService;   // handle to your service 
SERVICE_STATUS status;	// for QueryServiceStatus
BOOL retVal;
UINT_PTR IDT_TIMER1;
UINT_PTR IDT_TIMER2;

//Constants
const PTCHAR SERVICE_NAME = TEXT("MsgSender");   // provide any name you want
const PTCHAR SERVICE_DISPLAY_NAME = TEXT("Msg Sender"); // provide any display name
const PTCHAR SERVICE_TEXT_DESCRIPTION = TEXT("Writes time to file."); // provide any description
const int MSG_SHORTER_DURATION = 128;
const int MSG_LONGER_DURATION = 129;

int WINAPI WinMain(HINSTANCE hThisInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nShowCmd)
{
	HWND hwnd; // HANDLE to a window 
	MSG msg; // structure to hold a windows message
	WNDCLASSEX wcl; // to define windows class instance

					// define the windows class
	wcl.cbSize = sizeof(WNDCLASSEX);	// have to tell windows how big the structure is
										// cb = count of bytes 
	wcl.hInstance = hThisInst;			// handle to this instance
	wcl.lpszClassName = szWinName;		// window class name
	wcl.lpfnWndProc = WindowFunc;		// windows function 
	wcl.style = 0;						// default style

	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION); // standard icon
	wcl.hIconSm = LoadIcon(NULL, IDI_WINLOGO);   // small icon
												 // cursor style 
												 // try IDC_IBEAM, _ARROW, _CROSS, _WAIT
	wcl.hCursor = LoadCursor(NULL, IDC_CROSS);

	wcl.lpszMenuName = NULL;	// no menu
	wcl.cbClsExtra = 0;			// no extra information needed
	wcl.cbWndExtra = 0;			// no extra information needed

								// make sure the windows background is white
								// try BLACK_BRUSH, DKGRAY_, HOLLOW_, LTGRAY_
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);

	// register the windows class
	// ATOM is just a number, an unsigned short
	ATOM aClassID;

	// Click on RegisterClassEx and press F1 to see Help
	// the ATOM aClassID is an identifier for the class being registered
	aClassID = RegisterClassEx(&wcl);

	// If last action failed end this program
	if (aClassID == 0) return 0;

	// now create the window
	hwnd = CreateWindow(
		szWinName,				/* name of window class */
		TEXT("Dialog Tester"),  /* title of the window */
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,	/* x coordinate - let Windows decide */
		CW_USEDEFAULT,	/* y coordinate - let Windows decide */
		CW_USEDEFAULT,	/* width - let Windows decide */
		CW_USEDEFAULT,	/* height - let Windows decide */
		HWND_DESKTOP,	/* no parent window */
		NULL,			/* no menu */
		hThisInst,		/* handle to this instance of the program */
		NULL			/* no additional arguments */
	);

	// Win98 programming book used DialogBox function, not CreateDialog
	HWND hDialog = CreateDialog(hThisInst, MAKEINTRESOURCE(IDD_DIALOG1),
		hwnd, DialogFunc);

	if (hDialog != NULL) // success, so show the dialog box
	{
		ShowWindow(hDialog, SW_SHOW);
	}
	else
	{
		// **** ProfD Change L prefix to TEXT for "Warning!"
		MessageBox(hwnd, TEXT("CreateDialog returned NULL"),
			TEXT("Warning!"), MB_OK | MB_ICONINFORMATION);
	}

	// do not show main window, though we could it wanted to
	// by uncommeting the next two lines
	//ShowWindow(hwnd, nShowCmd);
	//UpdateWindow(hwnd);         // request that Windows send an update message to our window 

	// create the message loop

	//                 Which window in our application?
	//                 All windows, in this case, so we use NULL
	//                   |
	//                  \|/
	// GetMessage(&msg, HWIND, filter min, filter max)
	//                          0       ,   0 for all messages. No filter 

	// receive message from Windows. Returns zero when program terminates. 
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg); // translate keyboard message from virtual key to character key
		DispatchMessage(&msg);  // dispatch the message back to Windows
	}

	KillTimer(hwnd, IDT_TIMER1);
	KillTimer(hwnd, IDT_TIMER2);

	return msg.wParam; // contains code for when program terminates
}

// This function is called by Windows and is passed messages from the message queue
// This function is the function Windows uses to communicate with our program

// Params: first 4 members of the MSG structure

LRESULT CALLBACK WindowFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		// let windows process any messages we don't handle
		return DefWindowProc(hwnd, message, wParam, lParam);
	}


	return 0;
}

BOOL CALLBACK DialogFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	TCHAR temp[bufMaxElements] = TEXT("");
	TCHAR current[bufMaxElements] = TEXT("");
	LPCTSTR fileName;
	CHAR buff[80] = { 0 };
	BOOL updateButtons = false;

	// get a handle to the Service Control Manger (SCM)
	schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		sprintf_s(buff, 10, "OpenSCManager Error: 0x%x,\n", GetLastError());
		OutputDebugString(buff);
		return false;
	}

	//Open the service
	schService = OpenService(schSCManager, SERVICE_NAME, SERVICE_ALL_ACCESS);
	if (schService == NULL)
	{
		sprintf_s(buff, 10, "CreateService Error: 0x%x,\n", GetLastError());
		OutputDebugString(buff);
		CloseServiceHandle(schSCManager);
		return false;
	}

	//Get the status of the service
	retVal = QueryServiceStatus(schService, &status);
	if (!retVal)
	{
		sprintf_s(buff, 10, "QueryStatus failed 0x%x,\n", GetLastError());
		OutputDebugString(buff);
	}

	if (updateButtons)
	{
		switch (status.dwCurrentState)
		{
		case SERVICE_RUNNING:
			//Disable start and continue
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_START), FALSE);
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_CONTINUE), FALSE);
			//Enable stop and pause
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_STOP), TRUE);
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_PAUSE), TRUE);
			break;
		case SERVICE_STOPPED:
			//Disable stop, pause and continue
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_STOP), FALSE);
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_PAUSE), FALSE);
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_CONTINUE), FALSE);
			//Enable start
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_START), TRUE);
			break;
		case SERVICE_PAUSED:
			//Disable start and pause
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_START), FALSE);
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_PAUSE), FALSE);
			//Enable stop and continue
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_STOP), TRUE);
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_CONTINUE), TRUE);
			break;
		default:
			//Disable all
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_STOP), FALSE);
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_PAUSE), FALSE);
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_CONTINUE), FALSE);
			EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_SERVICE_START), FALSE);
			OutputDebugString("Unknown service status encountered");
			break;
		}
		updateButtons = false;
	}

	switch (message)
	{
	case WM_INITDIALOG:
		SetDlgItemText(hwnd, IDC_FNAME, SZ_FILE);
		SetDlgItemText(hwnd, IDC_SERVICE_STATE, "Stopped");
		return TRUE;

	case WM_COMMAND:

		switch (LOWORD(wParam))
		{
		case IDOK: // OK button ID
				   // **** ProfD - Changed L prefix to TEXT for "OK"
			MessageBox(hwnd, TEXT("OK Clicked"), TEXT("OK"), MB_OK);
			break;

		case IDCANCEL: // Cancel button ID
			PostQuitMessage(0);
			break;

		case IDC_BUTTON_START:
			//Create and start timer
			SetTimer(hwnd, IDT_TIMER1, 1000, NULL);
			break;

		case IDC_BUTTON_END:
			// output the filepath
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, "Stopped");
			//Stop timers
			KillTimer(hwnd, IDT_TIMER1);
			KillTimer(hwnd, IDT_TIMER2);
			break;

		case IDC_BUTTON_SERVICE_START:
			//Start the service
			retVal = StartService(schService, 0, NULL);
			if (!retVal)
			{
				sprintf_s(buff, 10, "StartService failed 0x%x,\n", GetLastError());
			}

			//Set status of service
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));
			
			//Enable/disbale buttons based on status
			updateButtons = TRUE;

			//Create and start timer
			SetTimer(hwnd, IDT_TIMER2, 500, NULL);
			break;

		case IDC_BUTTON_SERVICE_STOP:
			//Stop the service
			retVal = ControlService(schService, SERVICE_CONTROL_STOP, &status);
			if (!retVal)
			{
				sprintf_s(buff, 10, "StopService failed 0x%x,\n", GetLastError());
				OutputDebugString(buff);
			}

			//Set status of service
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));

			//Enable/disbale buttons based on status
			updateButtons = TRUE;

			//Stop timer
			KillTimer(hwnd, IDT_TIMER2);
			break;

		case IDC_BUTTON_SERVICE_PAUSE:
			retVal = ControlService(schService, SERVICE_CONTROL_PAUSE, &status);
			if (!retVal)
			{
				sprintf_s(buff, 10, "PauseService failed 0x%x,\n", GetLastError());
			}

			//Set status of service
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));

			//Enable/disbale buttons based on status
			updateButtons = TRUE;

			//Stop timer
			KillTimer(hwnd, IDT_TIMER2);
			break;

		case IDC_BUTTON_SERVICE_CONTINUE:
			//Start the service
			retVal = StartService(schService, 0, NULL);
			if (!retVal)
			{
				sprintf_s(buff, 10, "StartService failed 0x%x,\n", GetLastError());
			}

			//Set status of service
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));

			//Enable/disbale buttons based on status
			updateButtons = TRUE;

			//Create and start timer
			SetTimer(hwnd, IDT_TIMER2, 500, NULL);
			break;

		default:
			//Start the service
			retVal = StartService(schService, 0, NULL);
			if (!retVal)
			{
				sprintf_s(buff, 10, "StartService failed 0x%x,\n", GetLastError());
			}

			//Set status of service
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));

			//Enable/disbale buttons based on status
			updateButtons = TRUE;

			//Create and start timer
			SetTimer(hwnd, IDT_TIMER2, 500, NULL);
			break;
		}
		break;

	case WM_TIMER:
		
		if (IDT_TIMER1)
		{
			//Open the time file
			hFile = CreateFile(
				SZ_FILE,
				GENERIC_READ | GENERIC_WRITE,
				0,						// no sharing 
				NULL,					// no security
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL,
				NULL					// no template
			);

			//Set text box to an empty field
			SetDlgItemText(hwnd, IDC_TIME_READ, TEXT(""));

			//If file was not opened correctly display the cause of the error and clear the text field
			if (hFile == INVALID_HANDLE_VALUE)
			{
				if (GetLastError() == ERROR_FILE_NOT_FOUND)
				{

					OutputDebugStringA("File not found\n");
					SetDlgItemText(hwnd, IDC_TIME_READ, TEXT(""));
				}

				else
				{
					OutputDebugStringA("Other file create error\n");
					SetDlgItemText(hwnd, IDC_TIME_READ, TEXT(""));
				}
			}
			//Else file opened correctly
			//Read out the contents of the file
			else
			{
				CHAR buff[64] = { 0 };
				DWORD dwBytesRead = 0;
				//Check for an error when reading the file
				if (!ReadFile(hFile, buff, sizeof(buff) - 1, &dwBytesRead, NULL))
				{
					DWORD dwError = GetLastError();
					OutputDebugStringA("ERROR READING FILE");
				}
				else
				{
					buff[sizeof(buff) - 1] = '\0';
					SetDlgItemText(hwnd, IDC_TIME_READ, buff);
				}
			}
			//Close and delete the file
			//This allows companion fileWriter to create an updated time.txt
			CloseHandle(hFile);
			DeleteFile(TEXT("C:\\zing\\time.txt"));
			return 0;
		}

		if (IDT_TIMER2)
		{
			retVal = QueryServiceStatus(schService, &status);
			if (!retVal)
			{
				sprintf_s(buff, 10, "QueryStatus failed 0x%x,\n", GetLastError());
				OutputDebugString(buff);
			}

			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));

			return 0;
		}
		break;
	}

	return 0;
}

LPCSTR StatusToString(DWORD status)
{
	LPCSTR statusString;
	switch (status)
	{
	case SERVICE_RUNNING:
		statusString = "Running";
		break;
	case SERVICE_STOPPED:
		statusString = "Stopped";
		break;
	case SERVICE_PAUSED:
		statusString = "Paused";
		break;
	}
	return statusString;
}