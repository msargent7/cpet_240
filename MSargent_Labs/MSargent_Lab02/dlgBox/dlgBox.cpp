/*
Student Name: Michael Sargent
CP240 Lab Section:  CP240-2A
Date: 8/30/17
Lab Assignment: Lab01 - fileReader
Project Name: dlgBox
File Name: dlgBox.cpp
Description: This program is designed to work alongside with the provided fileWriter.exe. The program will check for the time.txt
			 created by the fileWriter.exe, read the contents of the file, and display the contents in the created GUI. The time.txt
			 file is read and displayed every second, accompanied by an audible beep from the computer.
Limitations: A timer had to be used to for reading the time.txt file and outputting the beep. Use of Sleep() was not permitted.
Credits: Starter code provided by Professor Dellafelice. MSDN used as primary reference for unknown windows functions.
*/


// **** ProfD - This code correctly builds as UNICODE or not UNICODE
// **** Do not use #ifdef UNICODE to #undef UNICODE #endif
// **** in this file
// **** Instead set UNICODE or unset UNICODE via 
// **** CPPBasicDlg Project properties and select either
// **** Use Multi-Byte Character set i.e. ASCII format
// **** or
// **** Use Unicode Character Set 
/*

Sample code adapted from "Windows 98 Programming From The Ground Up",
Herbert Schildt, Osborne McGraw-Hill, 1998.

Demonstrates use of dialog box.

Used dialog editor in VS2015 to create dialog box.
*/
// contains API function prototypes, various types, macros and definitions
#include <windows.h> 
#include <tchar.h>
// **** ProfD - Added for nonUNICODE _tsprintf_s to sprintf_s decode
#include <cstdio>		
#include "resource.h"  
#include <iostream>
#include <cstring>
#include <string>

using namespace std;

// function prototypes
LRESULT CALLBACK WindowFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
//		__stdcall
void WINAPI ServiceCtrlHandler(DWORD Opcode);
//Service Status to a string
LPCSTR StatusToString(DWORD status);

// every dialog box has its own callback function
BOOL CALLBACK DialogFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
//BOOL FileExists(LPCSTR szPath);
//BOOL FileExists_2(LPCSTR szPath);

// globals
// **** ProfD - resolves UNICODE LPCWCHAR and ASCII LPCHAR
const TCHAR *szWinName = TEXT("SampleWindow"); // name of window class
const unsigned int bufMaxElements = 80;
//BOOL bFileExists;
//DWORD dwBytesWritten = 0;
HANDLE hFile;
LPCSTR SZ_FILE = "C:\\zing\\time.txt";
LPCSTR SZ_PATH = "C:\\zing";
SERVICE_STATUS m_ServiceStatus;
SERVICE_STATUS_HANDLE m_ServiceStatusHandle;
BOOL g_bServiceRunning;
BOOL g_bServicePaused;
HANDLE g_hTerminateEvt;

//Constants
const PTCHAR SERVICE_NAME = TEXT("MsgSender");   // provide any name you want
const PTCHAR SERVICE_DISPLAY_NAME = TEXT("Msg Sender"); // provide any display name
const PTCHAR SERVICE_TEXT_DESCRIPTION = TEXT("Writes time to file."); // provide any description
const int MSG_SHORTER_DURATION = 128;
const int MSG_LONGER_DURATION = 129;


int WINAPI WinMain(HINSTANCE hThisInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nShowCmd)
{
	HWND hwnd; // HANDLE to a window 
	MSG msg; // structure to hold a windows message
	WNDCLASSEX wcl; // to define windows class instance

					// define the windows class
	wcl.cbSize = sizeof(WNDCLASSEX);	// have to tell windows how big the structure is
										// cb = count of bytes 
	wcl.hInstance = hThisInst;			// handle to this instance
	wcl.lpszClassName = szWinName;		// window class name
	wcl.lpfnWndProc = WindowFunc;		// windows function 
	wcl.style = 0;						// default style

	wcl.hIcon = LoadIcon(NULL, IDI_APPLICATION); // standard icon
	wcl.hIconSm = LoadIcon(NULL, IDI_WINLOGO);   // small icon
												 // cursor style 
												 // try IDC_IBEAM, _ARROW, _CROSS, _WAIT
	wcl.hCursor = LoadCursor(NULL, IDC_CROSS);

	wcl.lpszMenuName = NULL;	// no menu
	wcl.cbClsExtra = 0;			// no extra information needed
	wcl.cbWndExtra = 0;			// no extra information needed

								// make sure the windows background is white
								// try BLACK_BRUSH, DKGRAY_, HOLLOW_, LTGRAY_
	wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);

	// register the windows class
	// ATOM is just a number, an unsigned short
	ATOM aClassID;

	// Click on RegisterClassEx and press F1 to see Help
	// the ATOM aClassID is an identifier for the class being registered
	aClassID = RegisterClassEx(&wcl);

	// If last action failed end this program
	if (aClassID == 0) return 0;

	// now create the window
	hwnd = CreateWindow(
		szWinName,				/* name of window class */
		TEXT("Dialog Tester"),  /* title of the window */
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,	/* x coordinate - let Windows decide */
		CW_USEDEFAULT,	/* y coordinate - let Windows decide */
		CW_USEDEFAULT,	/* width - let Windows decide */
		CW_USEDEFAULT,	/* height - let Windows decide */
		HWND_DESKTOP,	/* no parent window */
		NULL,			/* no menu */
		hThisInst,		/* handle to this instance of the program */
		NULL			/* no additional arguments */
	);

	// Win98 programming book used DialogBox function, not CreateDialog
	HWND hDialog = CreateDialog(hThisInst, MAKEINTRESOURCE(IDD_DIALOG1),
		hwnd, DialogFunc);

	if (hDialog != NULL) // success, so show the dialog box
	{
		ShowWindow(hDialog, SW_SHOW);
	}
	else
	{
		// **** ProfD Change L prefix to TEXT for "Warning!"
		MessageBox(hwnd, TEXT("CreateDialog returned NULL"),
			TEXT("Warning!"), MB_OK | MB_ICONINFORMATION);
	}

	// do not show main window, though we could it wanted to
	// by uncommeting the next two lines
	//ShowWindow(hwnd, nShowCmd);
	//UpdateWindow(hwnd);         // request that Windows send an update message to our window 

	// create the message loop

	//                 Which window in our application?
	//                 All windows, in this case, so we use NULL
	//                   |
	//                  \|/
	// GetMessage(&msg, HWIND, filter min, filter max)
	//                          0       ,   0 for all messages. No filter 

	// receive message from Windows. Returns zero when program terminates. 
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg); // translate keyboard message from virtual key to character key
		DispatchMessage(&msg);  // dispatch the message back to Windows
	}

	KillTimer(hwnd, 1);

	return msg.wParam; // contains code for when program terminates

}

// This function is called by Windows and is passed messages from the message queue
// This function is the function Windows uses to communicate with our program

// Params: first 4 members of the MSG structure

LRESULT CALLBACK WindowFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		// let windows process any messages we don't handle
		return DefWindowProc(hwnd, message, wParam, lParam);
	}


	return 0;
}
/*
BOOL FileExists(LPCSTR szPath)
{
	DWORD dwAttributes;

	dwAttributes = GetFileAttributes(szPath);

	if (dwAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		return FALSE; // not a file 
	}

	if ((dwAttributes & INVALID_FILE_ATTRIBUTES) == FALSE)
	{
		return FALSE; // file does not exist
	}

	return TRUE; // Not a directory and file is valid, so file exists
}
*/

BOOL FileExists_2(LPCSTR szPath)
{
	HANDLE hFile = CreateFile(
		szPath,
		GENERIC_READ | GENERIC_WRITE,   // desired access
		0,								// no sharing
		NULL,							// no security 
		OPEN_EXISTING,					// file MUST exist
		FILE_ATTRIBUTE_NORMAL,
		NULL							// no template file 
	);



	if (GetLastError() == ERROR_FILE_NOT_FOUND)
	{
		CloseHandle(hFile);
		return FALSE;
	}

	CloseHandle(hFile);

	return true;
}

BOOL CALLBACK DialogFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	TCHAR temp[bufMaxElements] = TEXT("");
	TCHAR current[bufMaxElements] = TEXT("");
	LPCTSTR fileName;
	SC_HANDLE schSCManager; // handle to Service Control Manager
	SC_HANDLE schService;   // handle to your service 
	SERVICE_STATUS status;	// for QueryServiceStatus
	BOOL retVal;
	
	switch (message)
	{
		// not in Win98 programming book
	case WM_INITDIALOG:
		SetDlgItemText(hwnd, IDC_FNAME, SZ_FILE);

		// get a handle to the Service Control Manger (SCM)
		schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

		if (schSCManager == NULL)
		{
			// RMD added
			printf("OpenSCManager Error: 0x%x\n", GetLastError());
			return false;
		}

		schService = OpenService(schSCManager, SERVICE_NAME, SERVICE_ALL_ACCESS);

		if (schService == NULL)
		{
			// RMD added
			printf("CreateService Error: 0x%x\n", GetLastError());
			return false;
		}
		return TRUE;

	case WM_COMMAND:

		switch (LOWORD(wParam))
		{
		case IDOK: // OK button ID
				   // **** ProfD - Changed L prefix to TEXT for "OK"
			MessageBox(hwnd, TEXT("OK Clicked"), TEXT("OK"), MB_OK);
			break;
			
		case IDCANCEL: // Cancel button ID
			PostQuitMessage(0);
			break;

		case IDC_BUTTON_START:
			// extract the text from the text box
			//GetDlgItemText(hwnd, IDC_FNAME, temp, sizeof(temp) / sizeof(temp[0]));

			//fileName = temp;

			if (fileName != SZ_FILE)
			{
				//Stop timer
				KillTimer(hwnd, 1);
				break;
			}

			// output the filepath
			SetDlgItemText(hwnd, IDC_CURRENT_FILE, fileName);

			//Create and start timer
			SetTimer(hwnd, 1, 1000, NULL);

			break;

		case IDC_BUTTON_END:
			// output the filepath
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, "Stopped");
			//Stop timer
			KillTimer(hwnd, 1);
			break;

		case IDC_BUTTON_SERVICE_START:
			//Start the service
			retVal = StartService(schService, 0, NULL);
			if (!retVal)
			{
				printf("Error starting service: %d\n", GetLastError());
			}

			// output the service status
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));

			//Create and start timer
			SetTimer(hwnd, 1, 1000, NULL);
			break;

		case IDC_BUTTON_SERVICE_STOP:
			//Stop the service
			retVal = ControlService(schService, SERVICE_CONTROL_STOP, &status);
			if (!retVal)
			{
				printf("Error starting service: %d\n", GetLastError());
			}

			// output the filepath
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));
			//Stop timer
			KillTimer(hwnd, 1);

			//CloseServiceHandle(schSCManager);
			break;

		case IDC_BUTTON_SERVICE_PAUSE:
			//Pause the service
			retVal = ControlService(schService, SERVICE_CONTROL_PAUSE, &status);
			if (!retVal)
			{
				printf("Error starting service: %d\n", GetLastError());
			}

			// output the filepath
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));
			KillTimer(hwnd, 1);
			break;

		case IDC_BUTTON_SERVICE_CONTINUE:
			//Resume the service
			retVal = StartService(schService, 0, NULL);
			if (!retVal)
			{
				printf("Error starting service: %d\n", GetLastError());
			}

			// output the filepath
			SetDlgItemText(hwnd, IDC_SERVICE_STATE, StatusToString(status.dwCurrentState));
			SetTimer(hwnd, 1, 1000, NULL);
			break;
		}

		break;

	case WM_TIMER:
		//Trigger the beep
		MessageBeep(0);

		//Open the time file
		hFile = CreateFile(
			SZ_FILE,
			GENERIC_READ | GENERIC_WRITE,
			0,						// no sharing 
			NULL,					// no security
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL					// no template
		);

		//Set text box to an empty field
		SetDlgItemText(hwnd, IDC_TIME_READ, TEXT(""));

		//If file was not opened correctly display the cause of the error and clear the text field
		if (hFile == INVALID_HANDLE_VALUE)
		{
			if (GetLastError() == ERROR_FILE_NOT_FOUND)
			{

				OutputDebugStringA("File not found\n");
				SetDlgItemText(hwnd, IDC_TIME_READ, TEXT(""));
			}

			else
			{
				OutputDebugStringA("Other file create error\n");
				SetDlgItemText(hwnd, IDC_TIME_READ, TEXT(""));
			}
		}
		//Else file opened correctly
		//Read out the contents of the file
		else
		{
			CHAR buff[64] = { 0 };
			DWORD dwBytesRead = 0;
			//Check for an error when reading the file
			if (!ReadFile(hFile, buff, sizeof(buff) - 1, &dwBytesRead, NULL))
			{
				DWORD dwError = GetLastError();
				OutputDebugStringA("ERROR READING FILE");
			}
			else
			{
				buff[sizeof(buff) - 1] = '\0';
				SetDlgItemText(hwnd, IDC_TIME_READ, buff);
			}
		}
		//Close and delete the file
		//This allows companion fileWriter to create an updated time.txt
		CloseHandle(hFile);
		DeleteFile(TEXT("C:\\zing\\time.txt"));
		break;

	}// end switch LOWORD

	return 0;

}

LPCSTR StatusToString(DWORD status)
{
	LPCSTR statusString;
	switch (status)
	{
	case SERVICE_RUNNING:
		statusString = "Running";
		break;
	case SERVICE_STOPPED:
		statusString = "Stopped";
		break;
	case SERVICE_PAUSED:
		statusString = "Paused";
		break;
	}
	return statusString;
}