﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Model;
using Repository; 

namespace Service
{
    public class authorService
    {
        IRepository<author> _repository; 

        public authorService(AuthorRepository repository)
        {
            _repository = repository; 
        }

        public List<authorViewModel> getAllAuthors()
        {
            List<authorViewModel> authors = new List<authorViewModel>();

            List<author> auList = _repository.FindAll(); 

            foreach(author au in auList)
            {
                authors.Add(new authorViewModel(au._id, au._fname, au._lname, au._phone, au._address,
                                                au._city, au._state, au._zip, au._contract.ToString())); 
            }

            return authors; 
        }

        public void updateAuthor(author au)
        {
            // or pass in author view model and convert to author
            // should the UI layer be exposed to authors or author view model only ?????

            

        }

        public void createAuthor(author au)
        {

        }

        public void removeAuthor(author au)
        { 
}
    }

    public class authorViewModel
    {
        // the view model class is for creating a display friendly object
        // usually, class properites are converted to strings 

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ID { get; private set; }
        public string Name { get { return FirstName + " " + LastName; } }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Contract { get; set; }

        public authorViewModel(string id, string fname, string lname, string phone, string address,
                               string city, string state, string zip, string contract)
        {
            FirstName = fname;
            LastName = lname;
            ID = id;
            Phone = phone;
            Address = address;
            City = city;
            State = state;
            Zip = zip;
            Contract = contract;
        }
    }
}
