﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO; 

using Model; 

namespace Repository
{
    public interface IRepository<T>
    {
        List<T> FindAll(); 
    }

    public class AuthorRepository : IRepository<author>
    {
        const string FILE_PATH = @"C:\zing\authors.txt"; // @ = interpret string literally, no need for \\

        public List<author> FindAll()
        {
            List<author> authors = new List<author>();

            // using statement: object is correctly disposed of
            using (StreamReader reader = File.OpenText(FILE_PATH))
            {
                string line = null;

                line = reader.ReadLine(); // get line from file, or null at end of file
                int count = 0; 

                while (line != null)
                {
                    if(count > 2) // ignore first two lines 
                    {
                        string[] fields = line.Split(null as string[], StringSplitOptions.RemoveEmptyEntries);

                        if (fields[8] == "1")
                        {
                            author au = new author(fields[0], fields[2], fields[1], fields[3],
                                               fields[4], fields[5], fields[6], fields[7], true);
                            authors.Add(au);
                        }

                        else
                        {
                            author au = new author(fields[0], fields[2], fields[1], fields[3],
                                                   fields[4], fields[5], fields[6], fields[7], false);
                            authors.Add(au);
                        }
                        

                        //Console.WriteLine(System.Environment.NewLine); // print '\n'
                    }

                    line = reader.ReadLine(); // next line in file 
                    count++; 
                }
            }

            return authors; 
        }
    }
}
