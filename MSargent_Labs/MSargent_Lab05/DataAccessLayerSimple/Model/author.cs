﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class author
    {
        public string _id { get; private set; }
        public string _fname { get; private set; }
        public string _lname { get; private set; }
        public string _phone { get; private set; }
        public string _address { get; private set; }
        public string _city { get; private set; }
        public string _state { get; private set; }
        public string _zip { get; private set; }
        public bool _contract { get; private set; }

        public author(string au_id, string fname, string lname, string phone, string address,
                      string city, string state, string zip, bool contract)
        {
            this._id = au_id;
            this._fname = fname;
            this._lname = lname;
            this._phone = phone;
            this._address = address;
            this._city = city;
            this._state = state;
            this._zip = zip;
            this._contract = contract;
        }

        public string[] StringArray()
        {
            string[] information = new string[9];
            information[0] = this._id;
            information[1] = this._fname;
            information[2] = this._lname;
            information[3] = this._phone;
            information[4] = this._address;
            information[5] = this._city;
            information[6] = this._state;
            information[7] = this._zip;
            information[8] = this._contract.ToString();

            return information;
        }
    }
}
