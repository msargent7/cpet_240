﻿using System.Windows.Forms;

using System.Configuration;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


namespace UI
{
    public partial class Form1 : Form
    {
        private bool editButtonClicked = false;
        private bool newButtonClicked = false;
        private bool saveButtonClicked = false;

        public Form1()
        {
            InitializeComponent();

            loadListView();
        }

        public void loadListView()
        {
            //Set up the visuals for the list view.
            lvAuthors.View = View.Details;
            lvAuthors.GridLines = true;
            lvAuthors.FullRowSelect = true;

            //Create columns
            lvAuthors.Columns.Add("ID");
            lvAuthors.Columns.Add("Last Name");
            lvAuthors.Columns.Add("First Name");
            lvAuthors.Columns.Add("Phone");
            lvAuthors.Columns.Add("Address");
            lvAuthors.Columns.Add("City");
            lvAuthors.Columns.Add("State");
            lvAuthors.Columns.Add("Zip Code");
            lvAuthors.Columns.Add("Contract?");

            //Size columns
            lvAuthors.Columns[0].Width = -2;
            lvAuthors.Columns[1].Width = -2;
            lvAuthors.Columns[2].Width = -2;
            lvAuthors.Columns[3].Width = -2;
            lvAuthors.Columns[4].Width = -2;
            lvAuthors.Columns[5].Width = -2;
            lvAuthors.Columns[6].Width = -2;
            lvAuthors.Columns[7].Width = -2;
            lvAuthors.Columns[8].Width = -2;

            string author_file = getSetting("authorFile");

            // The using statement makes sure the object is correctly disposed of.
            using (StreamReader reader = File.OpenText(author_file))
            {
                string line = "";

                // Find the dashes
                while (!line.Trim().StartsWith("-")) line = reader.ReadLine();

                // The dashes tell us the width of a field.
                string[] dashes = line.Split();
                // Keep track of those widths.
                int[] counts = new int[dashes.Length];

                // Count up the dashes for every field.
                for (int i = 0; i < dashes.Length; i++)
                {
                    counts[i] = dashes[i].Length;
                }


                // Now read the data.
                line = reader.ReadLine();

                while (line != null)
                {
                    // Create a list view item using the first string.
                    ListViewItem lvi = new ListViewItem(line.Substring(0, counts[0]).Trim());

                    // Keep track of the current column.
                    int loc = counts[0];
                    foreach (int ct in counts.Skip(1))
                    {
                        string value = "";

                        // The last field isn't right padded, so we need to handle that carefully.
                        if (loc + ct + 1 < line.Length)
                            // https://msdn.microsoft.com/en-us/library/system.string.substring(v=vs.110).aspx
                            value = line.Substring(loc, ct + 1).Trim();
                        else
                            // https://msdn.microsoft.com/en-us/library/system.string.substring(v=vs.110).aspx
                            value = line.Substring(loc);

                        // Add the subitem to the list view.
                        lvi.SubItems.Add(value);

                        // Keep track of where we are.
                        loc += ct + 1;
                    }

                    // Add the list view item to the list view.
                    lvAuthors.Items.Add(lvi);

                    // Read the next line.
                    line = reader.ReadLine();
                }
            }
        }

        public string getSetting(string key)
        {
            // adapted from: 
            // https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
            // see app.config file for how to add the setting

            string setting = "";

            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                setting = appSettings[key] ?? "Not Found"; // if appSettings[key] != null, then appSettings[key], else "Not found"
            }
            catch (ConfigurationErrorsException)
            {
                setting = "ERROR: Not found";
            }

            return setting;
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            ListViewItem authorInfo = new ListViewItem();
            buttonSave.Enabled = true;

            if (authorInfo == null)
            {
                MessageBox.Show("Please select an author to edit");
            }

            else
            {
                authorInfo = lvAuthors.SelectedItems[0];
                string contract;
                editButtonClicked = true;

                //Enable the fields to be edited
                textBoxID.Enabled = false;
                textBoxID.ReadOnly = true;
                textBoxLName.Enabled = true;
                textBoxFName.Enabled = true;
                textBoxPhone.Enabled = true;
                textBoxAddress.Enabled = true;
                textBoxCity.Enabled = true;
                textBoxState.Enabled = true;
                textBoxZip.Enabled = true;
                radioButtonYes.Enabled = true;
                radioButtonNo.Enabled = true;

                //Pull in the information from the selected line
                textBoxID.Text = authorInfo.SubItems[0].Text;
                textBoxLName.Text = authorInfo.SubItems[1].Text;
                textBoxFName.Text = authorInfo.SubItems[2].Text;
                textBoxPhone.Text = authorInfo.SubItems[3].Text;
                textBoxAddress.Text = authorInfo.SubItems[4].Text;
                textBoxCity.Text = authorInfo.SubItems[5].Text;
                textBoxState.Text = authorInfo.SubItems[6].Text;
                textBoxZip.Text = authorInfo.SubItems[7].Text;
                contract = authorInfo.SubItems[8].Text;
                if (Convert.ToInt32(contract) == 1)
                {
                    radioButtonYes.Checked = true;
                }
                else
                {
                    radioButtonNo.Checked = true;
                }
            }
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            newButtonClicked = true;
            buttonSave.Enabled = true;

            //Enable the fields to be edited
            textBoxID.Enabled = true;
            textBoxID.ReadOnly = false;
            textBoxLName.Enabled = true;
            textBoxFName.Enabled = true;
            textBoxPhone.Enabled = true;
            textBoxAddress.Enabled = true;
            textBoxCity.Enabled = true;
            textBoxState.Enabled = true;
            textBoxZip.Enabled = true;
            radioButtonYes.Enabled = true;
            radioButtonNo.Enabled = true;

            //Clear the fields
            textBoxID.Text = "";
            textBoxLName.Text = "";
            textBoxFName.Text = "";
            textBoxPhone.Text = "";
            textBoxAddress.Text = "";
            textBoxCity.Text = "";
            textBoxState.Text = "";
            textBoxZip.Text = "";
            radioButtonYes.Checked = false;
            radioButtonNo.Checked = false;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            saveButtonClicked = true;
            ListViewItem authorInfo = new ListViewItem();
            bool errorOccured = false;

            //Disable the text boxes
            textBoxID.Enabled = false;
            textBoxID.ReadOnly = true;
            textBoxLName.Enabled = false;
            textBoxFName.Enabled = false;
            textBoxPhone.Enabled = false;
            textBoxAddress.Enabled = false;
            textBoxCity.Enabled = false;
            textBoxState.Enabled = false;
            textBoxZip.Enabled = false;
            radioButtonYes.Enabled = false;
            radioButtonNo.Enabled = false;

            //Check the format of the text boxes
            Match matchID = Regex.Match(textBoxID.Text, "^[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]$");
            Match matchPhone = Regex.Match(textBoxPhone.Text, "^[0-9][0-9][0-9] [0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$");
            Match matchState = Regex.Match(textBoxState.Text, "^[A-Z][A-Z]$");
            Match matchZip = Regex.Match(textBoxZip.Text, "^[0-9][0-9][0-9][0-9][0-9]$");

            if (!matchID.Success)
            {
                MessageBox.Show("Please enter the ID number as: \n" +
                    "###-##-####");
                errorOccured = true;
            }

            else if (!matchPhone.Success)
            {
                MessageBox.Show("Please enter the phone number as: \n" +
                    "### ###-####");
                errorOccured = true;
            }

            else if (!matchState.Success)
            {
                MessageBox.Show("Please enter the state as two letters.");
                errorOccured = true;
            }

            else if (!matchZip.Success)
            {
                MessageBox.Show("Please enter the zip code: \n" +
                    "#####");
                errorOccured = true;
            }

            //Check for empty fields
            if(string.IsNullOrWhiteSpace(textBoxID.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid ID");
                errorOccured = true;
            }

            else if(string.IsNullOrWhiteSpace(textBoxLName.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid last name");
                errorOccured = true;
            }

            else if(string.IsNullOrWhiteSpace(textBoxFName.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid first name");
                errorOccured = true;
            }

            else if(string.IsNullOrWhiteSpace(textBoxAddress.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid address");
                errorOccured = true;
            }

            else if(string.IsNullOrWhiteSpace(textBoxCity.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid city");
                errorOccured = true;
            }

            else if(string.IsNullOrWhiteSpace(textBoxState.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid state");
                errorOccured = true;
            }

            else if(radioButtonNo.Checked == false && radioButtonYes.Checked == false)
            {
                MessageBox.Show("Please check either yes or no for contract");
                errorOccured = true;
            }

            //If no errors save the new info
            if (!errorOccured)
            {
                //If edit button has been checked bring in the info
                if (editButtonClicked)
                {
                    authorInfo = lvAuthors.SelectedItems[0];
                    //Assign the new information to the list view
                    authorInfo.SubItems[0].Text = textBoxID.Text.Trim();
                    authorInfo.SubItems[1].Text = textBoxLName.Text.Trim();
                    authorInfo.SubItems[2].Text = textBoxFName.Text.Trim();
                    authorInfo.SubItems[3].Text = textBoxPhone.Text.Trim();
                    authorInfo.SubItems[4].Text = textBoxAddress.Text.Trim();
                    authorInfo.SubItems[5].Text = textBoxCity.Text.Trim();
                    authorInfo.SubItems[6].Text = textBoxState.Text.Trim();
                    authorInfo.SubItems[7].Text = textBoxZip.Text.Trim();
                    if (radioButtonYes.Checked)
                    {
                        authorInfo.SubItems[8].Text = " 1";
                    }
                    else
                    {
                        authorInfo.SubItems[8].Text = " 0";
                    }
                }

                //If edit button has not been checked then create new listview item
                else
                {
                    string[] row = {textBoxID.Text.Trim(), textBoxLName.Text.Trim(), textBoxFName.Text.Trim(),
                                textBoxPhone.Text.Trim(), textBoxAddress.Text.Trim(), textBoxCity.Text.Trim(),
                                textBoxState.Text.Trim(), textBoxZip.Text.Trim()};

                    var lvItem = new ListViewItem(row);

                    if (radioButtonYes.Checked)
                    {
                        lvItem.SubItems.Add(" 1");
                    }
                    else
                    {
                        lvItem.SubItems.Add(" 0");
                    }

                    lvAuthors.Items.Add(lvItem);
                }
                editButtonClicked = false;
                buttonSave.Enabled = false;
            }

            //If there was an error return fields to original state
            else
            {
                buttonEdit_Click(sender, e);
            }
        }

        private void lvAuthors_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            buttonEdit.Enabled = true;
        }

        private void textBoxID_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only numbers and proper punctiuation to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsNumber(pressedKey) ||
                    Char.IsControl(pressedKey) ||
                    e.KeyChar == '-')
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        private void textBoxLName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only letters and proper punction to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsLetter(pressedKey) ||
                    Char.IsWhiteSpace(pressedKey))
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        private void textBoxFName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only letters and proper punction to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsLetter(pressedKey) ||
                    Char.IsWhiteSpace(pressedKey))
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        private void textBoxPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only numbers and proper punctiuation to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsNumber(pressedKey) ||
                    Char.IsWhiteSpace(pressedKey) ||
                    Char.IsControl(pressedKey) ||
                    e.KeyChar == '-')
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        private void textBoxAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only letters and proper punction to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsLetter(pressedKey) ||
                    Char.IsWhiteSpace(pressedKey) ||
                    Char.IsNumber(pressedKey))
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        private void textBoxCity_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only letters and proper punction to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsLetter(pressedKey) ||
                    Char.IsWhiteSpace(pressedKey))
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        private void textBoxState_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only letters and proper punction to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsLetter(pressedKey) ||
                    Char.IsWhiteSpace(pressedKey))
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        private void textBoxZip_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All only letters and proper punction to be added
            Char pressedKey = e.KeyChar;
            e.Handled = true;
            if (Char.IsNumber(pressedKey))
            {
                e.Handled = false;
            }
            //Allow for the backspace key
            if (Char.IsControl(pressedKey))
            {
                e.Handled = false;
            }
        }

        //If edit button has been selected without saving
        //check to see if they want to exit or go back and save changes
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((editButtonClicked || newButtonClicked) && !saveButtonClicked)
            {
                DialogResult dialogResult = MessageBox.Show("Exit without saving changes?", "Really Close?", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    e.Cancel = false;
                }
                else if (dialogResult == DialogResult.No)
                {
                    e.Cancel = true;
                }

            }
        }
    }
}
