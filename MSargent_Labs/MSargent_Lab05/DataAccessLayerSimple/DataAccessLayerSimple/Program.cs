﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Repository;
using Model; 

namespace DataAccessLayerSimple
{
    class Program
    {
        static void Main(string[] args) 
        {
            AuthorRepository repository = new AuthorRepository  ();

            List<author> authors = repository.FindAll(); 

            foreach(author au in authors)
            {
                Console.WriteLine("{0} {1}", au._id, au._fname + " " + au._lname); 
            }
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }

    
}
