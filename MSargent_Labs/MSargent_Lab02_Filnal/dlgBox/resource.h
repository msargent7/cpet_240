//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDD_DIALOG1                     101
#define IDC_BUTTON_GET_STRING           1001
#define IDC_BUTTON_START                1001
#define IDC_FNAME                       1002
#define IDC_TIME_READ                   1003
#define IDC_BUTTON_END                  1004
#define IDC_BUTTON_SERVICE_START        1005
#define IDC_CURRENT_FILE                1006
#define IDC_BUTTON_SERVICE_STOP         1006
#define IDC_BUTTON_SERVICE_PAUSE        1007
#define IDC_BUTTON_SERVICE_CONTINUE     1008
#define IDC_FNAME2                      1009
#define IDC_SERVICE_STATE               1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
