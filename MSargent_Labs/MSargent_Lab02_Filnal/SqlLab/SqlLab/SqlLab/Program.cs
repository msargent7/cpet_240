﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Model;
using Service;
using Repository;

namespace SqlLab
{
    class Program
    {
        static List<authorViewModel> auVM;
        static authorService as1;
        static AuthorRepositoryDB ardb = new AuthorRepositoryDB();

        static void Main(string[] args)
        {
            as1 = new authorService(ardb);
            auVM = as1.getAllAuthors();
            authorViewModel avm = as1.getAuthorByID("abc");

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
