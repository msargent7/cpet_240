﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Configuration;
using Model;

using System.Data.SqlClient;

namespace Repository
{
    public class returnResult
    {
        public bool success { get; set; }
        public bool message { get; set; }
    }

    public class authorGetResult : returnResult
    {
        public List<author> authors { get; set; }
    }

    public class authorSetResult : returnResult
    {
        // nothing to add 
    }

    public interface IRepository<T>
    {
        List<T> FindAll();
        T FindByID(string id);
        bool Add(T x);
        bool Update(T x);
        bool Remove(T x);
    }

    class configFile
    {
        public static string getSetting(string key)
        {
            // adapted from: 
            // https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
            // see app.config file for how to add the setting

            string setting = "";

            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                setting = appSettings[key] ?? "Not Found"; // if appSettings[key] != null, then appSettings[key], else "Not found"
            }
            catch (ConfigurationErrorsException)
            {
                //setting = "ERROR: Not found";
            }

            return setting;
        }
    }


    public class AuthorRepositoryDB : IRepository<author>
    {
        private string _connectionString;

        public AuthorRepositoryDB()
        {
            _connectionString = configFile.getSetting("pubsDBConnectionString");
            if (_connectionString == "ERROR: Not found")
            {
                Environment.Exit(-1);
            }
        }

        public author FindByID(string id)
        {
            author au = null;

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM authors WHERE au_id = @id", connection))
                    {
                        command.Parameters.Add(new SqlParameter("@id", id));

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {

                                au = new author(dataReader["au_id"].ToString(),
                                                dataReader["au_fname"].ToString(),
                                                dataReader["au_lname"].ToString(),
                                                dataReader["phone"].ToString(),
                                                dataReader["address"].ToString(),
                                                dataReader["city"].ToString(),
                                                dataReader["state"].ToString(),
                                                dataReader["zip"].ToString(),
                                                (bool)dataReader["contract"]); // dataReader["contract"]); 
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(author);
            }

            return au;
        }

        public bool Add(author au)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "INSERT INTO authors VALUES(@id, @fname, @lname, @phone, @addr, @city, @state, @zip, @contract)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@id", au.id));
                        command.Parameters.Add(new SqlParameter("@fname", au.fname));
                        command.Parameters.Add(new SqlParameter("@lname", au.lname));
                        command.Parameters.Add(new SqlParameter("@phone", au.phone));
                        command.Parameters.Add(new SqlParameter("@addr", au.address));
                        command.Parameters.Add(new SqlParameter("@city", au.city));
                        command.Parameters.Add(new SqlParameter("@state", au.state));
                        command.Parameters.Add(new SqlParameter("@zip", au.zip));
                        command.Parameters.Add(new SqlParameter("@contract", au.contract));

                        if (command.ExecuteNonQuery() <= 0) return false;

                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool Update(author au)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "UPDATE authors SET  au_lname = @lname, au_fname = @fname, phone = @phone, " +
                                 "address = @addr, city = @city, state = @state, zip = @zip, contract = @contract " +
                                 "WHERE au_id = @id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@lname", au.lname);
                        command.Parameters.AddWithValue("@fname", au.fname);
                        command.Parameters.AddWithValue("@phone", au.phone);
                        command.Parameters.AddWithValue("@addr", au.address);
                        command.Parameters.AddWithValue("@city", au.city);
                        command.Parameters.AddWithValue("@state", au.state);
                        command.Parameters.AddWithValue("@zip", au.zip);
                        command.Parameters.AddWithValue("@contract", au.contract);
                        command.Parameters.AddWithValue("@id", au.id);

                        if (command.ExecuteNonQuery() <= 0) return false;
                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public bool Remove(author au)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM authors WHERE au_id = @id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", au.id);
                        command.ExecuteNonQuery();
                    }// end use command;
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }

        public List<author> FindAll()
        {
            List<author> authors = new List<author>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM authors", connection))
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                author au = new author(dataReader["au_id"].ToString(),
                                                        dataReader["au_fname"].ToString(),
                                                        dataReader["au_lname"].ToString(),
                                                        dataReader["phone"].ToString(),
                                                        dataReader["address"].ToString(),
                                                        dataReader["city"].ToString(),
                                                        dataReader["state"].ToString(),
                                                        dataReader["zip"].ToString(),
                                                        (bool)dataReader["contract"]); // dataReader["contract"]); 

                                authors.Add(au);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<author>);
            }

            return authors;
        }
    }

}
