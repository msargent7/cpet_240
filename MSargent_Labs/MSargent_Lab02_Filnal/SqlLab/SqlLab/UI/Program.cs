﻿/*
Student Name: Michael Sargent
CP240 Lab Section:  CP240-2A
Date: 10/18/17
Lab Assignment: Lab06 - SQL DAL
Project Name: SqlLab
File Name: Program.cpp
Description: This program is designed to read in data from a sql database, display those contents in a windows form, and allow
             the contents to be added to, edited, or removed
Limitations:
Credits: Starter code provided by Professor Dellafelice. MSDN used as primary reference for unknown windows functions.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
