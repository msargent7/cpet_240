﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Configuration;

namespace employeeProjects
{

    // NOTE: For this lab, the class is not abstract.  

    public class project
    {
        protected int m_id;
        protected string m_description;

        ObservableCollection<employee> _employees;

        public project()
        {

        }

        public project(int id, string description)
        {
            m_id = id;
            m_description = description;

            _employees = new ObservableCollection<employee>();
        }

        public int id
        {
            get { return m_id; }
        }

        public string description
        {
            get { return m_description; }
            set { m_description = value; }
        }

        public ObservableCollection<employee> employees
        {
            get { return _employees; }
            set { _employees = value; }
        }
    }

    public class department
    {
        protected string m_id;
        protected string m_description;

        public department()
        {

        }

        public department(string id, string description)
        {
            m_id = id;
            m_description = description;
        }

        public string id
        {
            get { return m_id; }
        }

        public string description
        {
            get { return m_description; }
            set { m_description = value; }
        }
    }

    public class employee
    {
        protected int m_id;
        protected string m_fname;
        protected string m_lname;
        protected DateTime m_dateOfHire;

        // future 
        protected string m_dept;
        protected bool m_contract;

        public employee()
        {

        }

        public employee(int id, string lname, string fname, string dept_id, DateTime dateOfHire, bool contract)
        {
            // business rule - id may never change 
            m_id = id;
            m_dateOfHire = dateOfHire;

            m_fname = fname;
            m_lname = lname;
            m_dept = dept_id;
            m_contract = contract;
        }

        public string lastName
        {
            get { return m_lname; }
            set { m_lname = value; }
        }

        public string firstName
        {
            get { return m_fname; }
            set { m_fname = value; }
        }

        public string name
        {
            get { return string.Format("{0} {1}", m_fname, m_lname); }
        }

        public int id
        {
            // business rule - id may never change, so readonly propertly
            get { return m_id; }
        }

        public DateTime dateOfHire
        {
            get { return m_dateOfHire; }
        }

        public string department
        {
            get { return m_dept; }
            set { m_dept = value; }
        }

        public bool contract
        {
            get { return m_contract; }
            set { m_contract = value; }
        }
    }
}