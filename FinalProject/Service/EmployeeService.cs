﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using employeeProjects;
using Lab09;
using System.Collections.ObjectModel;

namespace Service
{
    public class employeeService
    {
        IEmployeeRepository<employee> _repository;

        public employeeService(IEmployeeRepository<employee> repository)
        {
            _repository = repository;
        }

        public List<employeeViewModel> getAllEmployees()
        {
            List<employeeViewModel> employees = new List<employeeViewModel>();
            List<employee> emList = _repository.FindAll();

            foreach(employee em in emList)
            {
                employees.Add(viewModelFromEmployee(em));
            }

            return employees;
        }

        public employeeViewModel getEmployeeByID(string em_id)
        {
            employee em = _repository.FindByID(em_id);
            if (em == null)
                return null;
            return viewModelFromEmployee(em);
        }

        public bool addEmployee(employeeViewModel emVM)
        {
            employee em = VMToEmployee(emVM);

            if (_repository.FindByID(em.id.ToString()) == null)
                return _repository.Add(em);
            return false;
        }

        public bool updateEmployee(employeeViewModel emVM)
        {
            employee em = VMToEmployee(emVM);
            return _repository.Update(em);
        }

        public bool removeEmployee(employeeViewModel emVM)
        {
            employee em = VMToEmployee(emVM);
            return _repository.Remove(em);
        }

        public employeeViewModel viewModelFromEmployee(employee em)
        {
            employeeViewModel e = new employeeViewModel(em.id);

            e.EmployeeID = em.id;
            e.FirstName = em.firstName;
            e.LastName = em.lastName;
            e.Department = em.department;
            e.DateHired = em.dateOfHire;

            if (em.contract)
                e.Contract = "Yes";
            else
                e.Contract = "No";

            return e;
        }

        public employee VMToEmployee(employeeViewModel emvm)
        {
            Boolean contract = (emvm.Contract == "Yes");
            return new employee(emvm.EmployeeID, emvm.LastName, emvm.FirstName, emvm.Department, emvm.DateHired, contract);
        }
        
    }
    
    public class employeeViewModel
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateHired { get; set; }
        public string Department { get; set; }
        public string Contract { get; set; }

        public employeeViewModel(int e_id)
        {
            EmployeeID = e_id;
        }

        public employeeViewModel(int e_id, string lname, string fname, string department, DateTime hiredate, string contract)
        {
            EmployeeID = e_id;
            FirstName = fname;
            LastName = lname;
            Department = department;
            DateHired = hiredate;
            Contract = contract;

        }
    }

    public class projectService
    {
        IProjectRepository<project> _repository;

        public projectService(IProjectRepository<project> repository)
        {
            _repository = repository;
        }

        public List<projectViewModel> getAllProjects()
        {
            List<projectViewModel> projects = new List<projectViewModel>();
            List<project> prList = _repository.FindAll();

            foreach(project pr in prList)
            {
                projects.Add(viewModelFromProject(pr));
            }

            return projects;
        }

        public List<employeeViewModel> employeesOnProject(projectViewModel prVM)
        {
            project pr = VMToProject(prVM);
            List<employeeViewModel> employees = new List<employeeViewModel>();
            List<employee> emList = _repository.FindAll(pr);

            foreach (employee em in emList)
            {
                employees.Add(viewModelFromEmployee(em));
            }

            return employees;
        }

        public List<projectViewModel> projectsByEmployee(employeeViewModel emVM)
        {
            employee em = VMToEmployee(emVM);
            List<projectViewModel> projects = new List<projectViewModel>();
            List<project> prList = _repository.FindAll(em);

            foreach (project pr in prList)
            {
                projects.Add(viewModelFromProject(pr));
            }

            return projects;
        }

        public bool addEmployeeToProject(employeeViewModel emVM, projectViewModel prVM)
        {
            employee em = VMToEmployee(emVM);
            project pr = VMToProject(prVM);
            List<employee> emList = _repository.FindAll(pr);

            foreach(employee e in emList)
            {
                if (e.id == em.id)
                    return false;
            }

            return _repository.Add(em, pr);
        }

        public bool removeEmployeeFromProject(employeeViewModel emVM, projectViewModel prVM)
        {
            employee em = VMToEmployee(emVM);
            project pr = VMToProject(prVM);

            return _repository.Remove(em, pr);
        }

        public bool removeEmployeeFromTable(employeeViewModel emVM)
        {
            employee em = VMToEmployee(emVM);

            return _repository.Remove(em);
        }

        public bool addProject(projectViewModel prVM)
        {
            project pr = VMToProject(prVM);
            List<projectViewModel> projects = new List<projectViewModel>();
            List<project> prList = _repository.FindAll();

            foreach(project p in prList)
            {
                if (p.id == pr.id)
                    return false;
            }

            return _repository.Add(pr);
        }

        public bool updateProject(projectViewModel prVM)
        {
            project pr = VMToProject(prVM);
            return _repository.Update(pr);
        }

        public bool removeProject(projectViewModel prVM)
        {
            project pr = VMToProject(prVM);
            return _repository.Remove(pr);
        }

        public projectViewModel viewModelFromProject(project pr)
        {
            projectViewModel p = new projectViewModel(pr.id);

            p.ProjectID = pr.id;
            p.Description = pr.description;

            return p;
        }

        public project VMToProject(projectViewModel prVM)
        {
            return new project(prVM.ProjectID, prVM.Description);
        }

        public employeeViewModel viewModelFromEmployee(employee em)
        {
            employeeViewModel e = new employeeViewModel(em.id);

            e.EmployeeID = em.id;
            e.FirstName = em.firstName;
            e.LastName = em.lastName;
            e.Department = em.department;
            e.DateHired = em.dateOfHire;

            if (em.contract)
                e.Contract = "Yes";
            else
                e.Contract = "No";

            return e;
        }

        public employee VMToEmployee(employeeViewModel emvm)
        {
            Boolean contract = (emvm.Contract == "Yes");
            return new employee(emvm.EmployeeID, emvm.LastName, emvm.FirstName, emvm.Department, emvm.DateHired, contract);
        }
    }

    public class projectViewModel
    {
        public int ProjectID { get; set; }
        public string Description { get; set; }

        ObservableCollection<employee> employees;

        public projectViewModel(int p_id)
        {
            ProjectID = p_id;
        }
        public projectViewModel(int p_id, string description)
        {
            ProjectID = p_id;
            Description = description;
            employees = new ObservableCollection<employee>();
        }
    }

    public class departmentService
    {
        IDepartmentRepository<department> _repository;

        public departmentService(IDepartmentRepository<department> repository)
        {
            _repository = repository;
        }

        public List<departmentViewModel> getAllDepartments()
        {
            List<departmentViewModel> departments = new List<departmentViewModel>();
            List<department> dpList = _repository.FindAll();

            foreach (department dp in dpList)
            {
                departments.Add(viewModelFromDepartment(dp));
            }

            return departments;
        }

        public departmentViewModel viewModelFromDepartment(department dp)
        {
            departmentViewModel d = new departmentViewModel(dp.id);

            d.DeptID = dp.id;
            d.Description = dp.description;

            return d;
        }

        public department VMToDepartment(departmentViewModel dpVM)
        {
            return new department(dpVM.DeptID, dpVM.Description);
        }
    }

    public class departmentViewModel
    {
        public string DeptID { get; set; }
        public string Description { get; set; }

        ObservableCollection<employee> employees;

        public departmentViewModel(string dept_id)
        {
            DeptID = dept_id;
        }
        public departmentViewModel(string dept_id, string description)
        {
            DeptID = dept_id;
            Description = description;
        }
    }
}
