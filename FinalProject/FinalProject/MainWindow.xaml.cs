﻿/*
Student Name: Michael Sargent
CP240 Lab Section:  CP240-2A
Date: 12/14/17
Lab Assignment: Final Project
Project Name: FinalProject
File Name: FinalProject.sln
Description: This program is designed to interact with a SQL database. It pulls information about employees, projects
             and departments from this database, and displays that information in a WPF window. This program also allows
             for the creation, revision, and deletion of both employees and projects, along with adding and removing 
             employees to a selected project.
Limitations: 
Credits: 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Service;
using Lab09;
using System.Collections.ObjectModel;

namespace FinalProject
{
    public partial class MainWindow : Window
    {
        ObservableCollection<employeeViewModel> EmployeeCollection;
        ObservableCollection<projectViewModel> ProjectCollection;
        ObservableCollection<departmentViewModel> DepartmentCollection;
        List<employeeViewModel> EmployeeList;
        List<projectViewModel> ProjectList;
        List<departmentViewModel> DepartmentList;
        EmployeeRepositoryDB emRepository = new EmployeeRepositoryDB();
        ProjectRepositoryDB prRepository = new ProjectRepositoryDB();
        DepartmentRepositoryDB dpRepository = new DepartmentRepositoryDB();
        employeeService emService;
        projectService prService;
        departmentService dpService;
        employeeViewModel selectedEmployee;
        projectViewModel selectedProject;

        public MainWindow()
        {
            refreshList();
        }

        //Upon the selection change in the project list, the corresponding employees will displayed
        //in the Selected Project's Employees list
        private void dGridProjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateProjectButton.IsEnabled = true;
            RemoveProjectButton.IsEnabled = true;
            AddProjectButton.IsEnabled = false;

            if (dGridProjects.SelectedItem == null)
            {
                return;
            }

            DataGrid dg = sender as DataGrid;
            selectedProject = (projectViewModel)dg.SelectedItem;

            EmployeeList = prService.employeesOnProject(selectedProject);

            EmployeeCollection = new ObservableCollection<employeeViewModel>(EmployeeList);

            InitializeComponent();

            dGridEmployeesOnProject.ItemsSource = EmployeeCollection;
            dGridEmployeesOnProject.IsReadOnly = true;

            pIDTextBox.Text = selectedProject.ProjectID.ToString();
            descrTextBox.Text = selectedProject.Description;
        }
        
        //Upon the selection change in the employee list, the corresponding projects will displayed
        //in the Selected Employee's Projects list
        private void dGridEmployees_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateEmployeeButton.IsEnabled = true;
            RemoveEmployeeButton.IsEnabled = true;
            AddEmployeeButton.IsEnabled = false;

            if (dGridEmployees.SelectedItem == null)
            {
                return;
            }

            DataGrid dg = sender as DataGrid;
            selectedEmployee = (employeeViewModel)dg.SelectedItem;

            ProjectList = prService.projectsByEmployee(selectedEmployee);
            ProjectCollection = new ObservableCollection<projectViewModel>(ProjectList);

            InitializeComponent();

            dGridProjectsByEmployee.ItemsSource = ProjectCollection;
            dGridProjectsByEmployee.IsReadOnly = true;

            eIDTextBox.Text = selectedEmployee.EmployeeID.ToString();
            LastNameTextBox.Text = selectedEmployee.LastName;
            FirstNameTextBox.Text = selectedEmployee.FirstName;
            DateOfHireBox.SelectedDate = selectedEmployee.DateHired;
            departmentComboBox.SelectedValue = selectedEmployee.Department;

            if (selectedEmployee.Contract == "Yes")
                contractComboBox.SelectedValue = "Yes";

            else
                contractComboBox.SelectedValue = "No";

        }

        private void AddToProject_Click(object sender, RoutedEventArgs e)
        {
            //Make sure that an employee is selected from the list
            //If not, display error and return
            if (dGridEmployees.SelectedItem == null)
            {
                MessageBox.Show("Please select an employee" + Environment.NewLine +
                                "from the Employees list");
                return;
            }

            //Make sure that an project is selected from the list
            //If not, display error and return
            if (dGridProjects.SelectedItem == null)
            {
                MessageBox.Show("Please select an project" + Environment.NewLine +
                                "from the Projects list");
                return;
            }

            selectedEmployee = (employeeViewModel)dGridEmployees.SelectedItem;
            selectedProject = (projectViewModel)dGridProjects.SelectedItem;

            prService.addEmployeeToProject(selectedEmployee, selectedProject);


            refreshList();
        }

        //Remove the selected employee in the Selected Project's Employee list from the selected project
        private void RemoveFromProjectButton_Click(object sender, RoutedEventArgs e)
        {
            //Make sure that an employee is selected from the list
            //If not, display error and return
            if (dGridEmployeesOnProject.SelectedItem == null)
            {
                MessageBox.Show("Please select an employee to remove" + Environment.NewLine +
                                "from the Selected Project's Employees list");
                return;
            }

            selectedEmployee = (employeeViewModel)dGridEmployeesOnProject.SelectedItem;
            selectedProject = (projectViewModel)dGridProjects.SelectedItem;

            //Make sure that a project is selected from the list
            //If not, display error and return
            if (selectedProject == null)
            {
                MessageBox.Show("Please select a project" + Environment.NewLine +
                                "from the project list");
                return;
            }

            prService.removeEmployeeFromProject(selectedEmployee, selectedProject);

            refreshList();
        }

        //refreshList refreshes the four data grids.
        //It is called whenever a change is made to any of the grids
        private void refreshList()
        {
            emService = new employeeService(emRepository);
            prService = new projectService(prRepository);
            dpService = new departmentService(dpRepository);

            EmployeeList = emService.getAllEmployees();
            DepartmentList = dpService.getAllDepartments();

            //Using linq, the employee dept_id was matched with the department dept_id
            //The department name was then substituted for the dept_id in employee
            //resulting in the department name being displayed in the data grid
            foreach (employeeViewModel em in EmployeeList)
            {
                var name = DepartmentList.Where(c => c.DeptID == em.Department).FirstOrDefault();
                em.Department = (!string.IsNullOrEmpty(name.Description) ? name.Description : "");
            }

            if (emService != null)
            {


                if (EmployeeList != null)
                {
                    EmployeeCollection = new ObservableCollection<employeeViewModel>(EmployeeList);

                    InitializeComponent();

                    dGridEmployees.ItemsSource = EmployeeCollection;
                    dGridEmployees.IsReadOnly = true;
                    //The e_id is determined by sql based on the next available unique key
                    //The E-ID text box is populated with "Auto-generated" to pass this along to the user
                    eIDTextBox.Text = "Auto-generated";
                }
            }
            if (prService != null)
            {
                ProjectList = prService.getAllProjects();

                if (ProjectList != null)
                {
                    ProjectCollection = new ObservableCollection<projectViewModel>(ProjectList);

                    InitializeComponent();

                    dGridProjects.ItemsSource = ProjectCollection;
                    dGridProjects.IsReadOnly = true;
                    //The p_id is determined by sql based on the next available unique key
                    //The P-ID text box is populated with "Auto-generated" to pass this along to the user
                    pIDTextBox.Text = "Auto-generated";
                }
            }

            if (selectedEmployee != null)
            {
                ProjectList = prService.projectsByEmployee(selectedEmployee);
                ProjectCollection = new ObservableCollection<projectViewModel>(ProjectList);

                dGridProjectsByEmployee.ItemsSource = ProjectCollection;
                dGridProjectsByEmployee.IsReadOnly = true;
            }

            if (selectedProject != null)
            {
                InitializeComponent();

                dGridEmployeesOnProject.ItemsSource = EmployeeCollection;
                dGridEmployeesOnProject.IsReadOnly = true;
            }
        }

        //Check for empty fields in the employee information boxes
        //Called before adding or updating an employee
        private bool employeeErrorCheck()
        {
            bool errorOccured = false;

            if (string.IsNullOrWhiteSpace(eIDTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid E-ID");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(LastNameTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid last name");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(FirstNameTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid first name");
                errorOccured = true;
            }

            return errorOccured;
        }

        //Check for empty fields in the project information boxes
        //Called before adding or updating an project
        private bool projectErrorCheck()
        {
            bool errorOccured = false;

            if (string.IsNullOrWhiteSpace(pIDTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid P-ID");
                errorOccured = true;
            }

            else if (string.IsNullOrWhiteSpace(descrTextBox.Text.Trim()))
            {
                MessageBox.Show("Please enter a valid projet description");
                errorOccured = true;
            }

            return errorOccured;
        }

        private void QuitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //The clear button clears all information fields for both employees and projects
        //It also clears the two datagrids that display employee projects and project employees
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            AddEmployeeButton.IsEnabled = true;
            AddProjectButton.IsEnabled = true;
            UpdateEmployeeButton.IsEnabled = false;
            RemoveEmployeeButton.IsEnabled = false;
            UpdateProjectButton.IsEnabled = false;
            RemoveProjectButton.IsEnabled = false;

            eIDTextBox.Text = "";
            LastNameTextBox.Text = "";
            FirstNameTextBox.Text = "";
            DateOfHireBox.SelectedDate = null;
            departmentComboBox.SelectedValue = "Admin";
            contractComboBox.SelectedValue = "Yes";

            pIDTextBox.Text = "";
            descrTextBox.Text = "";

            refreshList();

            dGridEmployeesOnProject.ItemsSource = null;
            dGridProjectsByEmployee.ItemsSource = null;
        }

        //Add employee upon add employee button click
        private void AddEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            bool errorOccured = employeeErrorCheck();

            int eID = 1;
            DateTime dateHired = DateOfHireBox.SelectedDate.Value;

            //Check for any empty fields in the employee information boxes
            if (!errorOccured)
            {
                //Check for an employee contract in the combo box
                //If there is one, pass "Yes" as the argument for the employeeViewModel
                if (contractComboBox.Text == "Yes")
                {
                    //The following if statements check to see which department is selected from the combo box
                    //The corresponding department id is then passed as the argument for the employeeViewModel
                    if (departmentComboBox.Text == "Admin")
                    {
                        employeeViewModel emToAdd = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "AD001", dateHired, "Yes");
                        emService.addEmployee(emToAdd);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Engineering")
                    {
                        employeeViewModel emToAdd = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "EN001", dateHired, "Yes");
                        emService.addEmployee(emToAdd);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Sales")
                    {
                        employeeViewModel emToAdd = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "SA001", dateHired, "Yes");
                        emService.addEmployee(emToAdd);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Support")
                    {
                        employeeViewModel emToAdd = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "SU001", dateHired, "Yes");
                        emService.addEmployee(emToAdd);
                        refreshList();
                    }
                }

                //Check for an employee contract in the combo box
                //If there is one, pass "No" as the argument for the employeeViewModel
                if (contractComboBox.Text == "No")
                {
                    //The following if statements check to see which department is selected from the combo box
                    //The corresponding department id is then passed as the argument for the employeeViewModel
                    if (departmentComboBox.Text == "Admin")
                    {
                        employeeViewModel emToAdd = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "AD001", dateHired, "No");
                        emService.addEmployee(emToAdd);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Engineering")
                    {
                        employeeViewModel emToAdd = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "EN001", dateHired, "No");
                        emService.addEmployee(emToAdd);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Sales")
                    {
                        employeeViewModel emToAdd = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "SA001", dateHired, "No");
                        emService.addEmployee(emToAdd);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Support")
                    {
                        employeeViewModel emToAdd = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "SU001", dateHired, "No");
                        emService.addEmployee(emToAdd);
                        refreshList();
                    }
                }
            }

            ClearButton_Click(sender, e);
        }

        //Update employee upon update employee button click
        private void UpdateEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            bool errorOccured = employeeErrorCheck();

            int eID = Convert.ToInt32(eIDTextBox.Text);
            DateTime dateHired = DateOfHireBox.SelectedDate.Value;

            //Check for any empty fields in the employee information boxes
            if (!errorOccured)
            {
                //Check for an employee contract in the combo box
                //If there is one, pass "Yes" as the argument for the employeeViewModel
                if (contractComboBox.Text == "Yes")
                {
                    //The following if statements check to see which department is selected from the combo box
                    //The corresponding department id is then passed as the argument for the employeeViewModel
                    if (departmentComboBox.Text == "Admin")
                    {
                        employeeViewModel emToUpdate = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "AD001", dateHired, "Yes");
                        emService.updateEmployee(emToUpdate);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Engineering")
                    {
                        employeeViewModel emToUpdate = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "EN001", dateHired, "Yes");
                        emService.updateEmployee(emToUpdate);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Sales")
                    {
                        employeeViewModel emToUpdate = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "SA001", dateHired, "Yes");
                        emService.updateEmployee(emToUpdate);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Support")
                    {
                        employeeViewModel emToUpdate = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "SU001", dateHired, "Yes");
                        emService.updateEmployee(emToUpdate);
                        refreshList();
                    }
                }

                //Check for an employee contract in the combo box
                //If there is one, pass "Yes" as the argument for the employeeViewModel
                if (contractComboBox.Text == "No")
                {
                    //The following if statements check to see which department is selected from the combo box
                    //The corresponding department id is then passed as the argument for the employeeViewModel
                    if (departmentComboBox.Text == "Admin")
                    {
                        employeeViewModel emToUpdate = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "AD001", dateHired, "No");
                        emService.updateEmployee(emToUpdate);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Engineering")
                    {
                        employeeViewModel emToUpdate = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "EN001", dateHired, "No");
                        emService.updateEmployee(emToUpdate);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Sales")
                    {
                        employeeViewModel emToUpdate = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "SA001", dateHired, "No");
                        emService.updateEmployee(emToUpdate);
                        refreshList();
                    }
                    else if (departmentComboBox.Text == "Support")
                    {
                        employeeViewModel emToUpdate = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                             "SU001", dateHired, "No");
                        emService.updateEmployee(emToUpdate);
                        refreshList();
                    }
                }
            }

            ClearButton_Click(sender, e);
        }

        //Remove employee upon remove employee button click
        private void RemoveEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            int eID = Convert.ToInt32(eIDTextBox.Text);
            DateTime dateHired = DateOfHireBox.SelectedDate.Value;
            List<projectViewModel> prList;

            //Check for an employee contract in the combo box
            //If there is one, pass "Yes" as the argument for the employeeViewModel
            if (contractComboBox.Text == "Yes")
            {
                //The following if statements check to see which department is selected from the combo box
                //The corresponding department id is then passed as the argument for the employeeViewModel
                if (departmentComboBox.Text == "Admin")
                {
                    employeeViewModel emToRemove = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                         "AD001", dateHired, "Yes");

                    //Before an employee can be deleted, they must be removed from all projects
                    //The following foreach loop removes them from every project they are assigned to
                    prList = prService.projectsByEmployee(emToRemove);
                    foreach (projectViewModel pVM in prList)
                    {
                        prService.removeEmployeeFromProject(emToRemove, pVM);
                    }

                    emService.removeEmployee(emToRemove);
                    refreshList();
                }
                else if (departmentComboBox.Text == "Engineering")
                {
                    employeeViewModel emToRemove = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                         "EN001", dateHired, "Yes");

                    prList = prService.projectsByEmployee(emToRemove);
                    foreach (projectViewModel pVM in prList)
                    {
                        prService.removeEmployeeFromProject(emToRemove, pVM);
                    }

                    emService.removeEmployee(emToRemove);
                    refreshList();
                }
                else if (departmentComboBox.Text == "Sales")
                {
                    employeeViewModel emToRemove = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                         "SA001", dateHired, "Yes");

                    prList = prService.projectsByEmployee(emToRemove);
                    foreach (projectViewModel pVM in prList)
                    {
                        prService.removeEmployeeFromProject(emToRemove, pVM);
                    }

                    emService.removeEmployee(emToRemove);
                    refreshList();
                }
                else if (departmentComboBox.Text == "Support")
                {
                    employeeViewModel emToRemove = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                         "SU001", dateHired, "Yes");

                    prList = prService.projectsByEmployee(emToRemove);
                    foreach (projectViewModel pVM in prList)
                    {
                        prService.removeEmployeeFromProject(emToRemove, pVM);
                    }

                    emService.removeEmployee(emToRemove);
                    refreshList();
                }
            }

            //Check for an employee contract in the combo box
            //If there is one, pass "No" as the argument for the employeeViewModel
            if (contractComboBox.Text == "No")
            {
                //The following if statements check to see which department is selected from the combo box
                //The corresponding department id is then passed as the argument for the employeeViewModel
                if (departmentComboBox.Text == "Admin")
                {
                    employeeViewModel emToRemove = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                         "AD001", dateHired, "No");

                    //Before an employee can be deleted, they must be removed from all projects
                    //The following foreach loop removes them from every project they are assigned to
                    prList = prService.projectsByEmployee(emToRemove);
                    foreach (projectViewModel pVM in prList)
                    {
                        prService.removeEmployeeFromProject(emToRemove, pVM);
                    }

                    emService.removeEmployee(emToRemove);
                    refreshList();
                }
                else if (departmentComboBox.Text == "Engineering")
                {
                    employeeViewModel emToRemove = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                         "EN001", dateHired, "No");

                    prList = prService.projectsByEmployee(emToRemove);
                    foreach (projectViewModel pVM in prList)
                    {
                        prService.removeEmployeeFromProject(emToRemove, pVM);
                    }

                    emService.removeEmployee(emToRemove);
                    refreshList();
                }
                else if (departmentComboBox.Text == "Sales")
                {
                    employeeViewModel emToRemove = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                         "SA001", dateHired, "No");

                    prList = prService.projectsByEmployee(emToRemove);
                    foreach (projectViewModel pVM in prList)
                    {
                        prService.removeEmployeeFromProject(emToRemove, pVM);
                    }

                    emService.removeEmployee(emToRemove);
                    refreshList();
                }
                else if (departmentComboBox.Text == "Support")
                {
                    employeeViewModel emToRemove = new employeeViewModel(eID, LastNameTextBox.Text, FirstNameTextBox.Text,
                                                                         "SU001", dateHired, "No");

                    prList = prService.projectsByEmployee(emToRemove);
                    foreach (projectViewModel pVM in prList)
                    {
                        prService.removeEmployeeFromProject(emToRemove, pVM);
                    }

                    emService.removeEmployee(emToRemove);
                    refreshList();
                }
            }

            ClearButton_Click(sender, e);
        }

        //Add project upon add project button click
        private void AddProjectButton_Click(object sender, RoutedEventArgs e)
        {
            bool errorOccured = projectErrorCheck();

            int pID = 1;

            //Check for any empty fields in the project information fields
            if (!errorOccured)
            {
                projectViewModel prToAdd = new projectViewModel(pID, descrTextBox.Text);
                prService.addProject(prToAdd);
            }

            refreshList();
            ClearButton_Click(sender, e);
        }

        //Update project upon update project button click
        private void UpdateProjectButton_Click(object sender, RoutedEventArgs e)
        {
            bool errorOccured = projectErrorCheck();

            int pID = Convert.ToInt32(pIDTextBox.Text);

            //Check for any empty fields in the project information fields
            if (!errorOccured)
            {
                projectViewModel prToUpdate = new projectViewModel(pID, descrTextBox.Text);
                prService.updateProject(prToUpdate);
            }

            refreshList();
            ClearButton_Click(sender, e);
        }

        //Remove project upon remove project button click
        private void RemoveProjectButton_Click(object sender, RoutedEventArgs e)
        {
            int pID = Convert.ToInt32(pIDTextBox.Text);
            List<employeeViewModel> emList;

            projectViewModel prToRemove = new projectViewModel(pID, descrTextBox.Text);

            //Before a project can be deleted, all employees must be removed from that project
            //The following foreach loop removes each employee from the selected project
            emList = prService.employeesOnProject(prToRemove);
            foreach (employeeViewModel eVM in emList)
            {
                prService.removeEmployeeFromProject(eVM, prToRemove);
            }

            prService.removeProject(prToRemove);

            refreshList();
            ClearButton_Click(sender, e);
        }
    }
}
