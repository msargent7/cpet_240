﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using employeeProjects;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Configuration;


namespace Lab09
{
    public interface IEmployeeRepository<T>
    {
        List<employee> FindAll();   // Get all employees	** Required
        employee FindByID(string id);   // Find employee by ID  ** Required
        bool Add(employee x);       // Add employee  	** Extra credit
        bool Update(employee x);    // Modify employee	** Extra credit
        bool Remove(employee x);	// Remove employee	** Extra credit
    }

    public interface IProjectRepository<T>
    {
        List<project> FindAll();            // Get all projects				** Required project FindByID(string id);		// Find project by ID 
        List<employee> FindAll(project p);  // employees on a given project			** Required
        List<project> FindAll(employee e);  // projects assigned to an employee		** Extra credit
        bool Add(employee e, project p);    // add employee to project			** Required
        bool Remove(employee e, project p); // remove employee from project		** Required
        bool Remove(employee e);    // remove employee from employeeProject table  	** Extra credit		bool Remove(project p);	// Remove project from employeeProject table		** Extra credit    
        bool Add(project x);        // Add project 						** Extra credit
        bool Update(project x);     // Modify project					** Required
        bool Remove(project x);		// Remove project					** Extra credit
    }

    public interface IDepartmentRepository<T>
    {
        List<department> FindAll();
    }

    class configFile
    {
        public static string getSetting(string key)
        {
            // adapted from: 
            // https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
            // see app.config file for how to add the setting

            string setting = "";

            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                setting = appSettings[key] ?? "Not Found"; // if appSettings[key] != null, then appSettings[key], else "Not found"
            }
            catch (ConfigurationErrorsException)
            {
                //setting = "ERROR: Not found";
            }

            return setting;
        }
    }

    public class EmployeeRepositoryDB : IEmployeeRepository<employee>
    {
        private string _connectionString;

        public EmployeeRepositoryDB()
        {
            _connectionString = configFile.getSetting("departmentsDBConnectionString");
            if (_connectionString == "ERROR: Not found")
            {
                Environment.Exit(-1);
            }
        }
        public List<employee> FindAll()   // Get all employees	** Required
        {
            List<employee> employees = new List<employee>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM employees", connection))
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                employee em = new employee((int)dataReader["e_id"],
                                                        dataReader["lname"].ToString(),
                                                        dataReader["fname"].ToString(),
                                                        dataReader["dept_id"].ToString(),
                                                        (DateTime)dataReader["dateOfHire"],
                                                        (bool)dataReader["contract"]);

                                employees.Add(em);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<employee>);
            }

            return employees;
        }
        public employee FindByID(string id)   // Find employee by ID  ** Required
        {
            employee em = null;

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM authors WHERE e_id = @id", connection))
                    {
                        command.Parameters.Add(new SqlParameter("@id", id));

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {

                                em = new employee((int)dataReader["e_id"],
                                                        dataReader["lname"].ToString(),
                                                        dataReader["fname"].ToString(),
                                                        dataReader["dept_id"].ToString(),
                                                        (DateTime)dataReader["dateOfHire"],
                                                        (bool)dataReader["contract"]);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(employee);
            }

            return em;

        }
        public bool Add(employee em)      // Add employee  	** Extra credit
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "INSERT INTO employees values( @lname, @fname, @dept_id, @dateOfHire, @contract)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@lname", em.lastName));
                        command.Parameters.Add(new SqlParameter("@fname", em.firstName));
                        command.Parameters.Add(new SqlParameter("@dept_id", em.department));
                        command.Parameters.Add(new SqlParameter("@dateOfHire", em.dateOfHire));
                        command.Parameters.Add(new SqlParameter("@contract", em.contract));

                        if (command.ExecuteNonQuery() <= 0) return false;

                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }
        public bool Update(employee em)    // Modify employee	** Extra credit
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "UPDATE employees SET lname = @lname, fname = @fname, dept_id = @dept_id, " +
                                 "dateOfHire = @dateOfHire, contract = @contract " +
                                 "WHERE e_id = @e_id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@lname", em.lastName);
                        command.Parameters.AddWithValue("@fname", em.firstName);
                        command.Parameters.AddWithValue("@dept_id", em.department);
                        command.Parameters.AddWithValue("@dateOfHire", em.dateOfHire);
                        command.Parameters.AddWithValue("@contract", em.contract);
                        command.Parameters.AddWithValue("@e_id", em.id);

                        if (command.ExecuteNonQuery() <= 0) return false;
                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }
        public bool Remove(employee em) // Remove employee	** Extra credit
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM employees WHERE e_id = @id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", em.id);
                        command.ExecuteNonQuery();
                    }// end use command;
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }
    }


    public class ProjectRepositoryDB : IProjectRepository<project>
    {
        private string _connectionString;
        public ProjectRepositoryDB()
        {
            _connectionString = configFile.getSetting("departmentsDBConnectionString");
            if (_connectionString == "ERROR: Not found")
            {
                Environment.Exit(-1);
            }
        }
        public List<project> FindAll()            // Get all projects				** Required project FindByID(string id);		// Find project by ID 
        {
            List<project> projects = new List<project>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM projects", connection))
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                project pr = new project((int)dataReader["p_id"],
                                                        dataReader["p_descr"].ToString());

                                projects.Add(pr);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<project>);
            }

            return projects;
        }
        public List<employee> FindAll(project pr)  // employees on a given project			** Required
        {
            List<employee> employees = new List<employee>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "SELECT * FROM employees WHERE employees.e_id " +
                        "IN(SELECT employeeProjects.e_id FROM employeeProjects WHERE employeeProjects.p_id = @p_id)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@p_id", pr.id));

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                employee em = new employee((int)dataReader["e_id"],
                                                        dataReader["lname"].ToString(),
                                                        dataReader["fname"].ToString(),
                                                        dataReader["dept_id"].ToString(),
                                                        (DateTime)dataReader["dateOfHire"],
                                                        (bool)dataReader["contract"]);

                                employees.Add(em);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<employee>);
            }

            return employees;
        }
        public List<project> FindAll(employee em)  // projects assigned to an employee		** Extra credit
        {
            List<project> projects = new List<project>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT p.p_id, p.p_descr " +
                        "FROM employeeProjects ep " +
                        "JOIN projects p ON ep.p_id = p.p_id " +
                        "WHERE ep.e_id = @e_id", connection))
                    {
                        command.Parameters.Add(new SqlParameter("@e_id", em.id));

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                project pr = new project((int)dataReader["p_id"],
                                                        dataReader["p_descr"].ToString());

                                projects.Add(pr);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<project>);
            }

            return projects;
        }
        public bool Add(employee em, project pr)    // add employee to project			** Required
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "INSERT INTO employeeProjects values( @e_id, @p_id)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@e_id", em.id));
                        command.Parameters.Add(new SqlParameter("@p_id", pr.id));

                        if (command.ExecuteNonQuery() <= 0) return false;

                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }
        public bool Remove(employee em, project pr) // remove employee from project		** Required
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM employeeProjects WHERE e_id = @e_id AND p_id = @p_id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@e_id", em.id);
                        command.Parameters.AddWithValue("@p_id", pr.id);
                        command.ExecuteNonQuery();
                    }// end use command;
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }
        public bool Remove(employee em)    // remove employee from employeeProject table  	** Extra credit
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM employeeProjects WHERE e_id = @e_id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@e_id", em.id);
                        command.ExecuteNonQuery();
                    }// end use command;
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }
        public bool Add(project pr)        // Add project 						** Extra credit
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "INSERT INTO projects values(@p_descr)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@p_descr", pr.description));

                        if (command.ExecuteNonQuery() <= 0) return false;

                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }
        public bool Update(project pr)     // Modify project					** Required
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "UPDATE projects SET  p_descr = @p_descr " +
                                 "WHERE p_id = @p_id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@p_descr", pr.description);
                        command.Parameters.AddWithValue("@p_id", pr.id);

                        if (command.ExecuteNonQuery() <= 0) return false;
                    }// end use command
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }
        public bool Remove(project pr)      // Remove project					** Extra credit
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM projects WHERE p_id = @p_id";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@p_id", pr.id);
                        command.ExecuteNonQuery();
                    }// end use command;
                }// end use connection 
            }
            catch (SqlException ex)
            {
                return false;
            }

            return true;
        }
    }

    public class DepartmentRepositoryDB : IDepartmentRepository<department>
    {
        private string _connectionString;

        public DepartmentRepositoryDB()
        {
            _connectionString = configFile.getSetting("departmentsDBConnectionString");
            if (_connectionString == "ERROR: Not found")
            {
                Environment.Exit(-1);
            }
        }
        public List<department> FindAll()
        {
            List<department> departments = new List<department>();

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("SELECT * FROM dept", connection))
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read()) // foward only and readonly
                            {
                                department dp = new department(dataReader["dept_id"].ToString(),
                                                        dataReader["descr"].ToString());

                                departments.Add(dp);
                            } // end read loop 
                        }// end use reader
                    }// end use command
                }// end use connection 
            }
            catch (SqlException)
            {
                return default(List<department>);
            }

            return departments;
        }
    }
}
