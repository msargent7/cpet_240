﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Model;
using Repository;

namespace Service
{
    public class authorService
    {
        IRepository<author> _repository;

        public authorService(IRepository<author> repository)
        {
            _repository = repository;
        }

        public List<authorViewModel> getAllAuthors()
        {
            List<authorViewModel> authors = new List<authorViewModel>();

            List<author> auList = _repository.FindAll();

            foreach (author au in auList)
            {
                authors.Add(viewModelFromAuthor(au));
            }

            return authors;
        }

        public bool updateAuthor(authorViewModel auVM)
        {
            author au = VMToAuthor(auVM);
            // or pass in author view model and convert to author
            // should the UI layer be exposed to authors or author view model only ?????
            if (_repository.Update(au) == true)
            {
                return true;
            }

            return false;
        }

        public bool addAuthor(authorViewModel auVM)
        {
            author au = VMToAuthor(auVM);
            if (_repository.FindByID(au.id) == null)
            {
                return _repository.Add(au);
            }

            return false;
        }

        public bool removeAuthor(authorViewModel auVM)
        {
            author au = VMToAuthor(auVM);
            return _repository.Remove(au);

        }

        public authorViewModel getAuthorByID(string au_id)
        {
            author au = _repository.FindByID(au_id);
            if (au == null)
            {
                return null;
            }
            return viewModelFromAuthor(au);
        }

        public authorViewModel viewModelFromAuthor(author au)
        {
            authorViewModel a = new authorViewModel(au.id);

            a.FirstName = au.fname;
            a.LastName = au.lname;
            a.Phone = au.phone;
            a.Zip = au.zip;
            a.State = au.state;
            a.City = au.city;
            a.Address = au.address;

            if (au.contract) a.Contract = "Yes";
            else a.Contract = "No";

            return a;
        }

        public author VMToAuthor(authorViewModel auvm)
        {
            Boolean contract = (auvm.Contract == "Yes");
            return new author(auvm.ID, auvm.FirstName, auvm.LastName, auvm.Phone,
                                auvm.Address, auvm.City, auvm.State, auvm.Zip, contract);
        }

    }

    public class authorViewModel
    {
        // the view model class is for creating a display friendly object
        // usually, class properites are converted to strings 

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ID { get; set; }
        public string Name { get { return FirstName + " " + LastName; } }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Contract { get; set; }

        public authorViewModel()
        {

        }

        public authorViewModel(string id)
        {
            ID = id;
        }

        public authorViewModel(string id, string fname, string lname, string phone, string address,
                              string city, string state, string zip, string contract)
        {
            FirstName = fname;
            LastName = lname;
            ID = id;
            Phone = phone;
            Address = address;
            City = city;
            State = state;
            Zip = zip;
            Contract = contract;
        }
    }

    public class bookService
    {
        BookRepositoryDB _repository;

        public bookService(BookRepositoryDB repository)
        {
            _repository = repository;
        }

        public List<bookViewModel> getAllBooks()
        {
            List<bookViewModel> books = new List<bookViewModel>();

            List<book> bkList = _repository.FindAll();

            foreach (book bk in bkList)
            {
                books.Add(viewModelFromBook(bk));
            }

            return books;
        }

        public bool updateBook(bookViewModel bkVM)
        {
            book bk = VMToBook(bkVM);
            // or pass in author view model and convert to author
            // should the UI layer be exposed to authors or author view model only ?????
            if (_repository.Update(bk) == true)
            {
                return true;
            }

            return false;
        }

        public bool addToAuthor(string auID, string bookID)
        {
            if (_repository.AddToAuthor(auID, bookID))
            {
                return true;
            }

            return false;
        }

        public bool removeFromAuthor(string auID, string bookID)
        {
            return _repository.RemoveFromAuthor(auID, bookID);
        }

        public bool addBook(bookViewModel bkVM)
        {
            book bk = VMToBook(bkVM);
            if (_repository.Add(bk))
            {
                return true;
            }

            return false;
        }

        public bool removeBook(bookViewModel bkVM)
        {
            book bk = VMToBook(bkVM);
            return _repository.Remove(bk);

        }

        public bookViewModel getBookByID(string title_id)
        {
            book bk = _repository.FindByID(title_id);
            if (bk == null)
            {
                return null;
            }
            return viewModelFromBook(bk);
        }

        public List<bookViewModel> findByAuthor(string au_id)
        {
            List<bookViewModel> books = new List<bookViewModel>();

            List<book> bkList = _repository.FindByAuthor(au_id);

            foreach (book bk in bkList)
            {
                books.Add(viewModelFromBook(bk));
            }

            return books;
        }

        public bookViewModel viewModelFromBook(book bk)
        {
            bookViewModel b = new bookViewModel(bk.TitleID);

            b.TitleID = bk.TitleID;
            b.Title = bk.Title;
            b.Type = bk.Type;
            b.PubID = bk.PubID;
            b.Price = bk.Price.ToString();
            b.PubDate = bk.PubDate;

            return b;
        }

        public book VMToBook(bookViewModel bkVM)
        {
            return new book(bkVM.TitleID, bkVM.Title, bkVM.Type, bkVM.PubID, Decimal.Parse(bkVM.Price), bkVM.PubDate);
        }

    }

    public class bookViewModel
    {
        // the view model class is for creating a display friendly object
        // usually, class properites are converted to strings 

        public string TitleID { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string PubID { get; set; }
        public string Price { get; set; }
        public string PubDate { get; set; }


        public bookViewModel()
        {

        }
        public bookViewModel(string titleid)
        {
            TitleID = titleid;
        }

        public bookViewModel(string titleId, string title, string type, string pubid, string price,
                              string pubDate)
        {
            TitleID = titleId;
            Title = title;
            Type = type;
            PubID = pubid;
            Price = price;
            PubDate = pubDate;
        }
    }

    public class publisherService
    {
        IRepository<publisher> _repository;

        public publisherService(IRepository<publisher> repository)
        {
            _repository = repository;
        }

        public List<pubViewModel> getAllPublishers()
        {
            List<pubViewModel> pubs = new List<pubViewModel>();

            List<publisher> pubList = _repository.FindAll();

            foreach (publisher pub in pubList)
            {
                pubs.Add(viewModelFromPublisher(pub));
            }

            return pubs;
        }

        public bool updatePublisher(pubViewModel pubVM)
        {
            publisher pub = VMToPublisher(pubVM);
            // or pass in author view model and convert to author
            // should the UI layer be exposed to authors or author view model only ?????
            if (_repository.Update(pub) == true)
            {
                return true;
            }

            return false;
        }

        public bool addPublisher(pubViewModel pubVM)
        {
            publisher pub = VMToPublisher(pubVM);
            if (_repository.FindByID(pub.PubID) == null)
            {
                return _repository.Add(pub);
            }

            return false;
        }

        public bool removePublisher(pubViewModel pubVM)
        {
            publisher pub = VMToPublisher(pubVM);
            return _repository.Remove(pub);

        }

        public pubViewModel getPublisherByID(string pub_id)
        {
            publisher pub = _repository.FindByID(pub_id);
            if (pub == null)
            {
                return null;
            }
            return viewModelFromPublisher(pub);
        }

        public pubViewModel viewModelFromPublisher(publisher pub)
        {
            pubViewModel p = new pubViewModel(pub.PubID);

            p.PubID = pub.PubID;
            p.PubName = pub.PubName;
            p.City = pub.City;
            p.State = pub.State;
            p.Country = pub.Country;

            return p;
        }

        public publisher VMToPublisher(pubViewModel pubVM)
        {
            return new publisher(pubVM.PubID, pubVM.PubName, pubVM.City, pubVM.State, pubVM.Country);
        }

    }

    public class pubViewModel
    {
        // the view model class is for creating a display friendly object
        // usually, class properites are converted to strings 

        public string PubID { get; set; }
        public string PubName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public pubViewModel(string pubid)
        {
            PubID = pubid;
        }

        public pubViewModel(string pubid, string pubname, string city, string state, string country)
        {
            PubID = pubid;
            PubName = pubname;
            City = city;
            State = state;
            Country = country;
        }
    }
}
