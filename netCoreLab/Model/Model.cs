﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace Model
{
    public class author
    {
        private string _id;
        private string _fname;
        private string _lname;
        private string _phone;
        private string _address;
        private string _city;
        private string _state;
        private string _zip;
        private Boolean _contract;

        public author()
        {
            _id = _fname = _lname = _phone = _address = _city = _state = _zip = "";
            _contract = false;
        }

        public author(string au_id, string fname, string lname, string phone, string address, string city, string state, string zip, Boolean contract)
        {
            _id = au_id;
            _fname = fname;
            _lname = lname;
            _phone = phone;
            _address = address;
            _city = city;
            _state = state;
            _zip = zip;
            _contract = contract;
        }

        public string id
        {
            get { return _id; } // read only
            set { _id = value; }
        }

        public string fname
        {
            get { return _fname; }
            set { _fname = value; }
        }

        public string lname
        {
            get { return _lname; }
            set { _lname = value; }
        }

        public string name
        {
            get { return _fname + " " + _lname; }
        }



        public string phone
        {
            get { return _phone; }
            set
            {
                string temp = value;

                // cheesy, no shortcuts in regular expression, but it works
                Match match = Regex.Match(temp, "^[0-9][0-9][0-9] [0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$");

                if (match.Success)
                {
                    _phone = temp;
                }
                else
                {
                    throw new FormatException("### ###-####");
                }
            }
        }

        public string address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string city
        {
            get { return _city; }
            set { _city = value; }
        }

        public string state
        {
            get { return _state; }
            set { _state = value; } // could check for correct state here 
        }

        public string zip
        {
            get { return _zip; }
            set { _zip = value; }

        }

        public Boolean contract
        {
            get { return _contract; }
            set { _contract = value; }
        }
    }

    public class book
    {
        private string title_id;
        private string _title;
        private string _type;
        private string pub_id;
        private decimal? _price;
        private string pub_date;

        public book()
        {
            title_id = _title = _type = pub_id = pub_date = "";
            _price = 0;
        }

        public book(string TitleID, string Title, string Type, string PubID, decimal? Price, string PubDate)
        {
            title_id = TitleID;
            _title = Title;
            _type = Type;
            pub_id = PubID;
            _price = Price;
            pub_date = PubDate;
        }

        public string TitleID
        {
            get { return title_id; } // read only
            set { title_id = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string PubID
        {
            get { return pub_id; }
            set { pub_id = value; }
        }

        public decimal? Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public string PubDate
        {
            get { return pub_date; }
            set { pub_date = value; }
        }
    }

    public class publisher
    {
        private string pub_id;
        private string pub_name;
        private string city;
        private string state;
        private string country;

        public publisher()
        {
            pub_id = pub_name = city = state = country = "";
        }

        public publisher(string PubID, string PubName, string City, string State, string Country)
        {
            pub_id = PubID;
            pub_name = PubName;
            city = City;
            state = State;
            country = Country;
        }

        public string PubID
        {
            get { return pub_id; } // read only
            set { pub_id = value; }
        }

        public string PubName
        {
            get { return pub_name; }
            set { pub_name = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string State
        {
            get { return state; }
            set { state = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }
    }
}
