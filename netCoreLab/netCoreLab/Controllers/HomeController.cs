﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service;
using Model;
using Repository;
using System.Dynamic;

namespace netCoreLab.Controllers
{

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            Repository.AuthorRepositoryDB auRepo = new AuthorRepositoryDB();
            Repository.BookRepositoryDB bookRepo = new BookRepositoryDB();
            Repository.PublisherRepositoryDB pubRepo = new PublisherRepositoryDB();
            Service.authorService auService = new authorService(auRepo);
            Service.bookService bkService = new bookService(bookRepo);
            Service.publisherService pubService = new publisherService(pubRepo);

            ViewBag.Message = "Welcome to the Book Store Database!";
            dynamic mymodel = new ExpandoObject();
            mymodel.Authors = auService.getAllAuthors();
            mymodel.Books = bkService.getAllBooks();
            mymodel.Publishers = pubService.getAllPublishers();

            return View(mymodel);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Edit(string id)
        {
            authorService auService = new authorService(new AuthorRepositoryDB());
            List<authorViewModel> authors = auService.getAllAuthors();

            foreach (authorViewModel au in authors)
            {
                if (au.ID == id)
                    return View(au);
            }
            return View();
        }

        [HttpPost]
        public IActionResult Edit(authorViewModel au)
        {
            //update author
            authorService auService = new authorService(new AuthorRepositoryDB());
            auService.updateAuthor(au);
            
            return RedirectToAction("Index");
        }

        public IActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public IActionResult Create(authorViewModel au)
        {
            authorService auService = new authorService(new AuthorRepositoryDB());
            auService.addAuthor(au);

            return RedirectToAction("Index");
        }

        public IActionResult CreateBook()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateBook(bookViewModel bk)
        {
            bookService bkService = new bookService(new BookRepositoryDB());
            bkService.addBook(bk);

            return RedirectToAction("Index");
        }

        public IActionResult BooksByAuthor(string id)
        {
            bookService bkService = new bookService(new BookRepositoryDB());
            List<bookViewModel> books = bkService.findByAuthor(id);

            return View();
        }

        [HttpPost]
        public IActionResult BooksByAuthor()
        {
            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
